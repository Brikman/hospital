﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Hospital {
    public class Searcher {
        private DataGridView dataGridView;
        private DataGridViewColumn searchColumn;
        private DataGridViewCell result;
        private string search;

        private Thread thread;
        private ManualResetEvent sync_main;   // main thread
        private ManualResetEvent sync_thread; // search thread

        public  bool  Active;
        private bool  end;
        private int   index;
        private int   step;

        public Searcher() {
            Active = false;
        }

        public void Begin(DataGridView dataGridView, string column, string search) {
            Terminate();

            Active = true;
            end = false;
            index = -1;
            step = 1;

            this.dataGridView = dataGridView;
            this.search = search;

            searchColumn = null;
            foreach (DataGridViewColumn col in dataGridView.Columns) {
                if (col.Name.ToLower() == column) {
                    searchColumn = col;
                    break;
                }
            }
            if (searchColumn == null)
                throw new Exception("Критерий поиска \"" + column + "\" не найден");

            thread = new Thread(Search);
            thread.Start();
            sync_main   = new ManualResetEvent(false);
            sync_thread = new ManualResetEvent(false);
        }

        public DataGridViewCell FindNext() {
            step = 1;
            sync_thread.Set();
            sync_main.Reset();
            sync_main.WaitOne();
            
            return end ? null : result;
        }

        public DataGridViewCell FindPrev() {
            step = -1;
            sync_thread.Set();
            sync_main.Reset();
            sync_main.WaitOne();
            
            return end ? null : result;
        }

        private void Search() {
            Console.WriteLine("SEARCH BEGINS");
            while (true) {
                sync_thread.WaitOne();
                if (!Active)
                    break;

                end = false;
                while (true) {
                    index += step;
                    if (index < 0 || index >= dataGridView.Rows.Count) {
                        Console.WriteLine("TABLE END REACHED: index=" + index);
                        index -= 2 * step;
                        end = true;
                        break;
                    }
                    DataGridViewCell cell = dataGridView.Rows[index].Cells[searchColumn.Name];
                    Type type = cell.ValueType;
                    if (type == typeof(string) || type == typeof(DateTime)) {
                        if (cell.Value.ToString().Trim().ToLower().Contains(search)) {
                            result = cell;
                            break;
                        }
                    } else if (cell.Value.ToString().Trim().ToLower().Equals(search)) {
                        result = cell;
                        break;
                    }
                }
                
                sync_main.Set();
                sync_thread.Reset();
            }
            sync_main.Set();
            Console.WriteLine("SEARCH TERMINATED");
        }

        public void Terminate() {
            Active = false;
            if (sync_thread != null) {
                sync_thread.Set();
                sync_main.Reset();
                sync_main.WaitOne();
            }
        }
    }
}
