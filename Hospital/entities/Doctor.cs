﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital {
    public class Doctor : Entity {
        public int ID;
        public string Name;
        public string Spec;
        public string Date;
        public int Room;

        public Doctor(int id, string name, string spec, int room, string date) {
            ID = id;
            Name = name;
            Spec = spec;
            Room = room;
            Date = date;
        }
    }
}
