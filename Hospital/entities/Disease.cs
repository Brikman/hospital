﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital {
    public class Disease : Entity {
        public int ID;
        public int ID_Group;
        public string Name;
        public string[] Symptomes;

        public Disease(int id, string name) {
            ID = id;
            Name = name;
        }
    }
}
