﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital {
    public class Patient : Entity {
        public int ID;
        public string Name;
        public string Phone;
        public string BirthDate;
        public bool Sex;

        public Patient(int id, string name, bool sex, string phone, string date) {
            ID = id;
            Name = name;
            Sex = sex;
            Phone = phone;
            BirthDate = date;
        }
    }
}
