﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital {
    public class Visit : Entity {
        public int ID;
        public int IDPatient;
        public int IDDoctor;
        public string Date;
        public string Disease;
        public string Treatment;

        public Visit(int id, int id_patient, int id_doctor, string date) {
            ID = id;
            IDPatient = id_patient;
            IDDoctor = id_doctor;
            Date = date;
        }
    }
}
