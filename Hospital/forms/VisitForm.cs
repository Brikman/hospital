﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hospital {
    public enum VisitFormMode { 
        Preselected, 
        Common 
    };

    public partial class VisitForm : Form {
        MainForm form;
        DateTime now;
        Visit visit;

        List<Patient> patients = new List<Patient>();
        List<Doctor> doctors = new List<Doctor>();

        bool done = false;

        public VisitForm(MainForm form, List<Patient> patients, List<Doctor> doctors, VisitFormMode mode = VisitFormMode.Common) {
            InitializeComponent();
            this.patients = patients;
            this.doctors  = doctors;
            this.form = form;

            foreach (Doctor doctor in doctors) {
                doctorBox.Items.Add(doctor.Name + " ID:" + doctor.ID);
                if (!specBox.Items.Contains(doctor.Spec))
                    specBox.Items.Add(doctor.Spec);
            }

            specBox.SelectedIndex = 0;

            foreach (Patient patient in patients) {
                nameBox.Items.Add(patient.Name + " ID:" + patient.ID);
            }

            now = DateTime.Now;
            yearBox.Items.Add(now.Year);
            yearBox.Items.Add(now.Year + 1);
            yearBox.SelectedIndex = 0;

            if (mode == VisitFormMode.Preselected) {
                nameBox.SelectedIndex = 0;
                doctorBox.SelectedIndex = 0;

                nameBox.DropDownStyle = ComboBoxStyle.DropDownList;
                doctorBox.DropDownStyle = ComboBoxStyle.DropDownList;
                specBox.DropDownStyle = ComboBoxStyle.DropDownList;

                addPatientButton.Enabled = false;
            }
        }

        private void nameBox_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z || e.KeyData == Keys.Back || e.KeyData == Keys.Delete) {
                string name = nameBox.Text.Trim().ToLower();
                nameBox.Items.Clear();
                foreach (Patient patient in patients) {
                    if (patient.Name.ToLower().Contains(name)) 
                        nameBox.Items.Add(patient.Name + " ID:" + patient.ID);
                }

                nameBox.SelectionStart = name.Length;
                nameBox.DroppedDown = true;
                Cursor.Current = Cursors.Default;
            }
        }

        private void addPatientButton_Click(object sender, EventArgs e) {
            new PatientForm(form).AddPatient();
            form.UpdatePatientTab();
        }

        private void specBox_TextChanged(object sender, EventArgs e) {
            doctorBox.Text = "";
            doctorBox.Items.Clear();
            string spec = specBox.Text.Trim();

            foreach (Doctor doctor in doctors) {
                if (doctor.Spec.ToLower() == spec)
                    doctorBox.Items.Add(doctor.Name + " ID:" + doctor.ID);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            done = true;
            Close();
        }

        private void VisitForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (!done && checkFieldsFilled()) {
                DialogResult result = MessageBox.Show("Сохранить изменения?", "Выход", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes) {
                    saveButton.PerformClick();
                } else if (result == DialogResult.Cancel) {
                    e.Cancel = true;
                } 
            }
        }

        private void monthBox_SelectedIndexChanged(object sender, EventArgs e) {
            dayBox.Items.Clear();
            int year = Int32.Parse(yearBox.Text);
            int month = Int32.Parse(monthBox.Text);
            int min_day = (year == now.Year && month == now.Month) ? now.Day : 1;
            int max_day = DateTime.DaysInMonth(year, month);
            for (int i = min_day; i <= max_day; i++)
                dayBox.Items.Add(i);
            dayBox.SelectedIndex = 0;
        }

        private void yearBox_SelectedIndexChanged(object sender, EventArgs e) {
            monthBox.Items.Clear();
            int year = Int32.Parse(yearBox.Text);
            int month = (year == now.Year) ? now.Month : 1;
            for (int i = month; i <= 12; i++)
                monthBox.Items.Add(i);
            monthBox.SelectedIndex = 0;
        }

        private void saveButton_Click(object sender, EventArgs e) {
            nameBox_Leave(null, null);
            doctorBox_Leave(null, null);
            if (nameBox.BackColor != Color.White) {
                MessageBox.Show("Пациент отсутствует в базе", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (doctorBox.BackColor != Color.White) {
                MessageBox.Show("Сотрудник отсутствует в базе", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string patient = nameBox.Text.Trim();
            string doctor = doctorBox.Text.Trim();

            int id_patient = Int32.Parse(patient.Substring(patient.LastIndexOf(' ') + 4));
            int id_doctor = Int32.Parse(doctor.Substring(doctor.LastIndexOf(' ') + 4));
            string date = dayBox.Text + "-" + monthBox.Text + "-" + yearBox.Text;

            visit = new Visit(-1, id_patient, id_doctor, date);
            form.AddVisit(visit);
            form.UpdateVisitTab();
            done = true;
            Close();
        }

        private bool checkFieldsFilled() {
            List<ComboBox> boxes = panel.Controls.OfType<ComboBox>().ToList();
            foreach (ComboBox box in boxes) {
                if (box.Text == "") {
                    return false;
                }
            }
            return true;
        }

        private bool checkPatientBox() {
            Regex regex = new Regex(".+ID: *[0-9]+$");
            string text = nameBox.Text.Trim();
            if (!regex.IsMatch(text))
                return false;

            int i = text.LastIndexOf(' ') + 1;
            int id = Int32.Parse(text.Substring(i + 3));
            string name = text.Remove(i - 1).Trim();

            foreach (Patient patient in patients) {
                if (id == patient.ID && name == patient.Name)
                    return true;
            }
            return false;
        }

        private bool checkDoctorBox() {
            Regex regex = new Regex(".+ID: *[0-9]+$");
            string text = doctorBox.Text.Trim();
            if (!regex.IsMatch(text))
                return false;

            int i = text.LastIndexOf(' ') + 1;
            int id = Int32.Parse(text.Substring(i + 3));
            string name = text.Remove(i - 1).Trim();

            foreach (Doctor doctor in doctors) {
                if (id == doctor.ID && name == doctor.Name)
                    return true;
            }
            return false;
        }

        private void nameBox_Leave(object sender, EventArgs e) {
            if (!checkPatientBox())
                nameBox.BackColor = Color.LightSalmon;
            else
                nameBox.BackColor = Color.White;
        }

        private void doctorBox_Leave(object sender, EventArgs e) {
            if (!checkDoctorBox())
                doctorBox.BackColor = Color.LightSalmon;
            else
                doctorBox.BackColor = Color.White;
        }

        private void nameBox_Enter(object sender, EventArgs e) {
            nameBox.BackColor = Color.White;
        }

        private void doctorBox_Enter(object sender, EventArgs e) {
            doctorBox.BackColor = Color.White;
        }
    }
}
