﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace Hospital {
    public partial class DocumentForm : Form {
        DateTime now = DateTime.Now;
        NpgsqlConnection conn;

        List<Patient> patients = new List<Patient>();
        List<Doctor>  doctors  = new List<Doctor>();

        List<Panel> panels = new List<Panel>();

        Dictionary<int, string> discr = new Dictionary<int, string> {
            {0, "Выдаётся пациенту. Содержит информацию о посещениях за указанный период."},
            {1, "Выдаётся врачу. Содержит информацию о приянтых пациентах за указанный период."},
            {2, "Содержит информацию о приёмах для врачей указанной специальности."},
            {3, "Содержит информацию о распространённости болезней."},
        };

        public DocumentForm(NpgsqlConnection conn) {
            InitializeComponent();
            this.conn = conn;

            try {
                string query;
                query = "select * from patients";
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();
                conn.Open();
                adapter.Fill(set);
                patientBox.Items.Clear();
                foreach (DataRow row in set.Tables[0].Rows) {
                    int id = (int)row.ItemArray[0];
                    string name = ((string)row.ItemArray[1]).Trim();
                    bool   sex = (bool)row.ItemArray[2];
                    string date = ((DateTime)row.ItemArray[3]).ToShortDateString();
                    string phone = ((string)row.ItemArray[4]).Trim();

                    patients.Add(new Patient(id, name, sex, phone, date));
                    patientBox.Items.Add(name + " ID:" + id);
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally {
                conn.Close();
            }

            try {
                string query;
                query = "select * from doctors_view";
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();
                conn.Open();
                adapter.Fill(set);
                doctorBox.Items.Clear();
                specBox.Items.Clear();
                foreach (DataRow row in set.Tables[0].Rows) {
                    int id = (int)row.ItemArray[0];
                    string name = ((string)row.ItemArray[1]).Trim();
                    int room = (int)row.ItemArray[2];
                    string spec = ((string)row.ItemArray[3]).Trim();
                    string date = ((DateTime)row.ItemArray[4]).ToShortDateString();

                    doctors.Add(new Doctor(id, name, spec, room, date));
                    doctorBox.Items.Add(name + " ID:" + id);

                    if (!specBox.Items.Contains(spec)) 
                        specBox.Items.Add(spec);
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally {
                conn.Close();
            }

            for (int year = 2000; year <= now.Year + 1; year++) {
                fromYear.Items.Add(year);
                toYear.Items.Add(year);
            }
            fromYear.SelectedIndex = 0;
            toYear.SelectedIndex = toYear.Items.IndexOf(now.Year);
            for (int month = 1; month < 12; month++) {
                fromMonth.Items.Add(month);
                toMonth.Items.Add(month);
            }
            fromMonth.SelectedIndex = 0;
            toMonth.SelectedIndex = 0;

            panels.Add(form1Panel);
            panels.Add(form2Panel);
            panels.Add(form3Panel);
            panels.Add(form4Panel);
            panels.Add(emptyPanel);
            panels.Add(datePanel);
            foreach (Panel panel in panels)
                panel.Visible = false;
            emptyPanel.Visible = true;
        }

        private void patientBox_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z || e.KeyData == Keys.Back || e.KeyData == Keys.Delete) {
                string name = patientBox.Text.Trim().ToLower();
                patientBox.Items.Clear();
                foreach (Patient patient in patients) {
                    if (patient.Name.ToLower().Contains(name))
                        patientBox.Items.Add(patient.Name + " ID:" + patient.ID);
                }
                patientBox.SelectionStart = name.Length;
                patientBox.DroppedDown = true;
                Cursor.Current = Cursors.Default;
            }
        }

        private void doctorBox_KeyUp(object sender, KeyEventArgs e) {
            if (e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z || e.KeyData == Keys.Back || e.KeyData == Keys.Delete) {
                string name = doctorBox.Text.Trim().ToLower();
                doctorBox.Items.Clear();
                foreach (Doctor doctor in doctors) {
                    if (doctor.Name.ToLower().Contains(name))
                        doctorBox.Items.Add(doctor.Name + " ID:" + doctor.ID);
                }

                doctorBox.SelectionStart = name.Length;
                doctorBox.DroppedDown = true;
                Cursor.Current = Cursors.Default;
            }
        }

        private void formBox_SelectedIndexChanged(object sender, EventArgs e) {
            issueButton.Enabled = true;
            string discription;
            int index = formBox.SelectedIndex;
            if (discr.TryGetValue(index, out discription)) {
                foreach (Panel panel in panels)
                    panel.Visible = false;
                panels[index].Visible = true;
                if (index != 3)
                    datePanel.Visible = true;
                patientBox.BackColor = Color.White;
                doctorBox.BackColor = Color.White;
                specBox.BackColor = Color.White;

                infoBox.Text = "Справка " + formBox.Text + ". " + discription;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            Close();
        }

        private void issueButton_Click(object sender, EventArgs e) {
            patientBox_Leave(null, null);
            doctorBox_Leave(null, null);
            specBox_Leave(null, null);

            bool valid = true;
            if (patientBox.Visible)
                valid = patientBox.BackColor == Color.White;
            else if (doctorBox.Visible)
                valid = doctorBox.BackColor == Color.White;
            else if (specBox.Visible)
                valid = specBox.BackColor == Color.White;

            if (!valid) {
                MessageBox.Show("Некорректные данные. Проверьте обязательные поля", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string text = "";
            int index = formBox.SelectedIndex;
            if (index == 0) {
                text = GetForm1();
            } else if (index == 1) {
                text = GetForm2();
            } else if (index == 2) {
                text = GetForm3();
            } else if (index == 3) {
                text = GetForm4();
            }

            new DocumentPreviewForm(formBox.Text, text).ShowDialog();
        }

        private string GetForm1() {
            StringBuilder text = new StringBuilder();
            string fromDate = fromDay.Text + "-" + fromMonth.Text + "-" + fromYear.Text;
            string toDate = toDay.Text + "-" + toMonth.Text + "-" + toYear.Text;

            int id = Int32.Parse(patientBox.Text.Substring(patientBox.Text.LastIndexOf(' ') + 4));
            string name = patientBox.Text.Remove(patientBox.Text.LastIndexOf(' '));
            try {
                string query = "select * from visits_view where id in " +
                               "(select id from visits where id_patient=" + id + ") " +
                               "and date between '" + fromDate + "' and '" + toDate + "'";
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();
                conn.Open();
                adapter.Fill(set);

                text.Append(String.Format("Пациент:\t{0}\nID:\t\t{1}\n\nЗарегистрированные посещения с {2} по {3}: {4}\n\n", 
                    name, id, fromDate, toDate, set.Tables[0].Rows.Count));
                text.Append("--------------------------------------------------------------------------------\n");
                text.Append(String.Format("{0, -5}{1, -35}{2, -15}{3, -10}{4, -20}\n", "ID", "Доктор", "Дата", "Статус", "Диагноз"));
                text.Append("--------------------------------------------------------------------------------\n");
                foreach (DataRow row in set.Tables[0].Rows) {
                    int ID = (int)row.ItemArray[0];
                    string doctor = ((string)row.ItemArray[2]).Trim();
                    string date = ((DateTime)row.ItemArray[3]).ToShortDateString();
                    string status = ((string)row.ItemArray[4]).Trim();
                    string disease = (row.ItemArray[5] != DBNull.Value) ? ((string)row.ItemArray[5]).Trim() : "";

                    text.Append(String.Format("{0, -5}{1, -35}{2, -15}{3, -10}{4, -20}\n", ID, doctor, date, status, disease));
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally {
                conn.Close();
            }

            return text.ToString();
        }

        private string GetForm2() {
            StringBuilder text = new StringBuilder();
            string fromDate = fromDay.Text + "-" + fromMonth.Text + "-" + fromYear.Text;
            string toDate = toDay.Text + "-" + toMonth.Text + "-" + toYear.Text;

            int id = Int32.Parse(doctorBox.Text.Substring(doctorBox.Text.LastIndexOf(' ') + 4));
            Doctor doctor = null;
            foreach (Doctor doc in doctors) {
                if (doc.ID == id) {
                    doctor = doc;
                    break;
                }
            }
            string name = doctor.Name;
            string spec = doctor.Spec;
            try {
                string query = "select * from visits_view where id in " +
                               "(select id from visits where id_doctor=" + id + ") " +
                               "and date between '" + fromDate + "' and '" + toDate + "'";
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();
                conn.Open();
                adapter.Fill(set);

                text.Append(String.Format("Доктор:\t{0}\nID:\t\t{1}\nСпециальность:\t{2}\n\nПринято пациентов с {3} по {4}: {5}\n\n", 
                    name, id, spec, fromDate, toDate, set.Tables[0].Rows.Count));
                text.Append("--------------------------------------------------------------------------------\n");
                text.Append(String.Format("{0, -5}{1, -35}{2, -15}{3, -10}{4, -20}\n", "ID", "Пациент", "Дата", "Статус", "Диагноз"));
                text.Append("--------------------------------------------------------------------------------\n");
                foreach (DataRow row in set.Tables[0].Rows) {
                    int ID = (int)row.ItemArray[0];
                    string patient = ((string)row.ItemArray[1]).Trim();
                    string date = ((DateTime)row.ItemArray[3]).ToShortDateString();
                    string status = ((string)row.ItemArray[4]).Trim();
                    string disease = (row.ItemArray[5] != DBNull.Value) ? ((string)row.ItemArray[5]).Trim() : "";

                    text.Append(String.Format("{0, -5}{1, -35}{2, -15}{3, -10}{4, -20}\n", ID, patient, date, status, disease));
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally {
                conn.Close();
            }

            return text.ToString();
        }

        private string GetForm3() {
            StringBuilder text = new StringBuilder();
            string fromDate = fromDay.Text + "-" + fromMonth.Text + "-" + fromYear.Text;
            string toDate = toDay.Text + "-" + toMonth.Text + "-" + toYear.Text;

            string spec = specBox.Text.Trim();
            
            try {
                string query = "select * from visits_view where id in " +
                               "(select id from visits where id_doctor in " +
                               "(select id from doctors_view where specialty='" + spec + "'))" +
                               "and date between '" + fromDate + "' and '" + toDate + "' order by doctor";
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();
                conn.Open();
                adapter.Fill(set);
                query = "select count(distinct(id)) from doctors_view where specialty='" + spec + "'";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);
                long amount_doctors = (long)command.ExecuteScalar();

                text.Append("----------------------------------------------------------------------------------------\n");
                text.Append(String.Format("Специальность:\t{0}\nДоктора:\t{1}\n\nПринято пациентов с {2} по {3}: {4}\n\n",
                    spec, amount_doctors, fromDate, toDate, set.Tables[0].Rows.Count));
                text.Append(String.Format("{0, -5}{1, -35}{2, -35}{3, -15}\n", "ID", "Доктор", "Пациент", "Дата"));
                
                string s = null;
                foreach (DataRow row in set.Tables[0].Rows) {
                    int ID = (int)row.ItemArray[0];
                    string patient = ((string)row.ItemArray[1]).Trim();
                    string doctor = ((string)row.ItemArray[2]).Trim();
                    string date = ((DateTime)row.ItemArray[3]).ToShortDateString();

                    if (s != doctor) {
                        s = doctor;
                        text.Append("----------------------------------------------------------------------------------------\n");
                    }
                    text.Append(String.Format("{0, -5}{1, -35}{2, -35}{3, -15}\n", ID, doctor, patient, date));
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally {
                conn.Close();
            }

            return text.ToString();
        }

        private string GetForm4() {
            StringBuilder text = new StringBuilder();

            try {
                string query = "select v.id_disease, d.name, count(v.id_disease) as amount " + 
                               "from visits v join diseases d on v.id_disease=d.id group by v.id_disease, d.name order by amount desc";
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();
                conn.Open();
                adapter.Fill(set);

                text.Append(String.Format("Зарегистрированных заболеваний:\t{0}\n\nПЕРЕЧЕНЬ ЗАБОЛЕВАНИЙ ПО РАСПРОСТРАНЁННОСТИ\n\n", set.Tables[0].Rows.Count));
                text.Append("----------------------------------------------\n");
                text.Append(String.Format("{0, -5}{1, -30}{2, -5}\n", "ID", "Название", "Количество"));
                text.Append("----------------------------------------------\n");
                string s = null;
                foreach (DataRow row in set.Tables[0].Rows) {
                    int ID = (int)row.ItemArray[0];
                    if (ID < 0)
                        continue;
                    string name = ((string)row.ItemArray[1]).Trim();
                    long amount = (long)row.ItemArray[2];

                    text.Append(String.Format("{0, -5}{1, -30}{2, -5}\n", ID, name, amount));
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally {
                conn.Close();
            }

            return text.ToString();
        }

        private void doctorInfoButton_Click(object sender, EventArgs e) {
            string text = doctorBox.Text;
            try {
                int id = Int32.Parse(text.Substring(text.LastIndexOf(' ') + 4));
                foreach (Doctor doctor in doctors) {
                    if (doctor.ID == id) {
                        new EntityInfoForm(doctor).ShowDialog();
                        break;
                    }
                }
            } catch {
            }
        }

        private void patientInfoButton_Click(object sender, EventArgs e) {
            string text = patientBox.Text;
            try {
                int id = Int32.Parse(text.Substring(text.LastIndexOf(' ') + 4));
                foreach (Patient patient in patients) {
                    if (patient.ID == id) {
                        new EntityInfoForm(patient).ShowDialog();
                        break;
                    }
                }
            } catch {
            }
        }

        //-----------------------DATE PICK----------------------------------------
        private void fromYear_SelectedIndexChanged(object sender, EventArgs e) {
            //int from_month = (fromMonth.Text != "") ? Int32.Parse(fromMonth.Text) : 1;
            //int from_year = Int32.Parse(fromYear.Text);
            ////int max_month = (from_year == now.Year) ? now.Month : 12;
            //int max_month = 12;
            //fromMonth.Items.Clear();
            //for (int i = 1; i <= max_month; i++)
            //    fromMonth.Items.Add(i);
            ////if (from_month > max_month)
            ////    from_month = max_month;
            //fromMonth.SelectedIndex = fromMonth.Items.IndexOf(from_month);

            //if (toYear.Text != "") {
            //    int to_year = Int32.Parse(toYear.Text);
            //    toYear.Items.Clear();
            //    for (int i = from_year; i <= now.Year; i++)
            //        toYear.Items.Add(i);
            //    if (from_year > to_year)
            //        to_year = from_year;
            //    toYear.SelectedIndex = toYear.Items.IndexOf(to_year);
            //}
            int from_month = (fromMonth.Text != "") ? Int32.Parse(fromMonth.Text) : 1;
            fromMonth.SelectedIndex = -1;
            fromMonth.SelectedIndex = fromMonth.Items.IndexOf(from_month);
        }

        private void fromMonth_SelectedIndexChanged(object sender, EventArgs e) {
            //int from_month = Int32.Parse(fromMonth.Text);
            //int from_year = Int32.Parse(fromYear.Text);
            //int from_day = (fromDay.Text != "") ? Int32.Parse(fromDay.Text) : 1;
            //int max_day =
            //    (from_year == now.Year && from_month == now.Month) ?
            //    now.Day :
            //    DateTime.DaysInMonth(from_year, from_month);
            //fromDay.Items.Clear();
            //for (int i = 1; i <= max_day; i++)
            //    fromDay.Items.Add(i);
            //fromDay.SelectedIndex = fromDay.Items.IndexOf(from_day);

            //if (toMonth.Text != "" && toYear.Text != "") {
            //    int to_month = Int32.Parse(toMonth.Text);
            //    int to_year = Int32.Parse(toYear.Text);

            //    toMonth.Items.Clear();
            //    int min_month = (from_year == to_year) ? from_month : 1;
            //    int max_month = (to_year == now.Year) ? now.Month : 12;

            //    for (int i = min_month; i <= max_month; i++)
            //        toMonth.Items.Add(i);

            //    if (from_year == to_year && from_month > to_month)
            //        to_month = from_month;
            //    toMonth.SelectedIndex = toMonth.Items.IndexOf(to_month);
            //}
            int from_year = Int32.Parse(fromYear.Text);
            int from_month = (fromMonth.Text != "") ? Int32.Parse(fromMonth.Text) : 1;
            int from_day = (fromDay.Text != "") ? Int32.Parse(fromDay.Text) : 1;
            int max_day = (from_year == now.Year && from_month == now.Month) ? 
                now.Day : 
                DateTime.DaysInMonth(from_year, from_month);
            fromDay.Items.Clear();
            for (int i = 1; i <= max_day; i++)
                fromDay.Items.Add(i);
            if (!fromDay.Items.Contains(from_day))
                from_day = 1;
            fromDay.SelectedIndex = fromDay.Items.IndexOf(from_day);
        }

        private void fromDay_SelectedIndexChanged(object sender, EventArgs e) {
                        //if (toYear.Text != "" && toMonth.Text != "" && toDay.Text != "") {
            //    int from_year = Int32.Parse(fromYear.Text);
            //    int from_month = Int32.Parse(fromMonth.Text);
            //    int from_day = Int32.Parse(fromDay.Text);

            //    int to_year = Int32.Parse(toYear.Text);
            //    int to_month = Int32.Parse(toMonth.Text);
            //    int to_day = Int32.Parse(toDay.Text);

            //    if (from_year == to_year && from_month == to_month) {
            //        toDay.Items.Clear();
            //        int max_day = DateTime.DaysInMonth(to_year, to_month);
            //        int min_day = from_day;
            //        for (int i = min_day; i <= max_day; i++)
            //            toDay.Items.Add(i);
            //        if (to_day < min_day)
            //            to_day = min_day;
            //        toDay.SelectedIndex = toDay.Items.IndexOf(to_day);
            //    }
            //}
        }

        private void toYear_SelectedIndexChanged(object sender, EventArgs e) {
                        //int from_year = Int32.Parse(fromYear.Text);
            //int from_month = Int32.Parse(fromMonth.Text);
            //int to_year = Int32.Parse(toYear.Text);
            //int to_month = (toMonth.Text != "") ? Int32.Parse(toMonth.Text) : 1;
            //int min_month = (from_year == to_year) ? from_month : 1;
            //int max_month = (to_year == now.Year) ? now.Month : 12;
            //toMonth.Items.Clear();
            //for (int i = min_month; i <= max_month; i++)
            //    toMonth.Items.Add(i);
            //if (to_month > max_month)
            //    to_month = max_month;
            //toMonth.SelectedIndex = toMonth.Items.IndexOf(to_month);
            int to_month = (toMonth.Text != "") ? Int32.Parse(toMonth.Text) : 1;
            toMonth.SelectedIndex = -1;
            toMonth.SelectedIndex = toMonth.Items.IndexOf(to_month);
        }

        private void toMonth_SelectedIndexChanged(object sender, EventArgs e) {
            //int from_year = Int32.Parse(fromYear.Text);
            //int from_month = Int32.Parse(fromMonth.Text);
            //int from_day = Int32.Parse(fromDay.Text);
            //int to_month = Int32.Parse(toMonth.Text);
            //int to_year = Int32.Parse(toYear.Text);
            //int to_day = (toDay.Text != "") ? Int32.Parse(toDay.Text) : 1;
            //int max_day =
            //    (to_year == now.Year && to_month == now.Month) ?
            //    now.Day :
            //    DateTime.DaysInMonth(to_year, to_month);
            //int min_day = (from_year == to_year && from_month == to_month) ? from_day : 1;
            //toDay.Items.Clear();
            //for (int i = min_day; i <= max_day; i++)
            //    toDay.Items.Add(i);
            //if (to_day < min_day)
            //    to_day = min_day;
            //toDay.SelectedIndex = toDay.Items.IndexOf(to_day);
            int to_year = Int32.Parse(toYear.Text);
            int to_month = (toMonth.Text != "") ? Int32.Parse(toMonth.Text) : 1;
            int to_day = (toDay.Text != "") ? Int32.Parse(toDay.Text) : 1;
            int max_day = (to_year == now.Year && to_month == now.Month) ?
                now.Day :
                DateTime.DaysInMonth(to_year, to_month);
            toDay.Items.Clear();
            for (int i = 1; i <= max_day; i++)
                toDay.Items.Add(i);
            if (!toDay.Items.Contains(to_day))
                to_day = 1;
            toDay.SelectedIndex = toDay.Items.IndexOf(to_day);
        }
        
        //-----------------------VALIDATION------------------------------------
        private void patientBox_Enter(object sender, EventArgs e) {
            patientBox.BackColor = Color.White;
        }

        private void patientBox_Leave(object sender, EventArgs e) {
            Regex regex = new Regex(".+ID: *[0-9]+$");
            string text = patientBox.Text.Trim();

            patientBox.BackColor = Color.White;
            if (regex.IsMatch(text)) {
                int i = text.LastIndexOf(' ') + 1;
                int id = Int32.Parse(text.Substring(i + 3));
                string name = text.Remove(i - 1).Trim();

                foreach (Patient patient in patients) {
                    if (id == patient.ID && name == patient.Name)
                        return;
                }
            } 
            patientBox.BackColor = Color.LightSalmon;  
        }

        private void doctorBox_Enter(object sender, EventArgs e) {
            doctorBox.BackColor = Color.White;
        }

        private void doctorBox_Leave(object sender, EventArgs e) {
            Regex regex = new Regex(".+ID: *[0-9]+$");
            string text = doctorBox.Text.Trim();

            doctorBox.BackColor = Color.White;
            if (regex.IsMatch(text)) {
                int i = text.LastIndexOf(' ') + 1;
                int id = Int32.Parse(text.Substring(i + 3));
                string name = text.Remove(i - 1).Trim();

                foreach (Doctor doctor in doctors) {
                    if (id == doctor.ID && name == doctor.Name)
                        return;
                }
            }
            doctorBox.BackColor = Color.LightSalmon;
        }

        private void specBox_Enter(object sender, EventArgs e) {
            specBox.BackColor = Color.White;
        }

        private void specBox_Leave(object sender, EventArgs e) {
            specBox.BackColor = Color.White;
            if (!specBox.Items.Contains(specBox.Text))
                specBox.BackColor = Color.LightSalmon;
        }
    }
}
