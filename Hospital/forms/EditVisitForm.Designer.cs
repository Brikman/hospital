﻿namespace Hospital {
    partial class EditVisitForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.patientBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.doctorBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.doctorInfoButton = new System.Windows.Forms.Button();
            this.dateBox = new System.Windows.Forms.TextBox();
            this.patientInfoButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.treatmentBox = new System.Windows.Forms.RichTextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.diseaseBox = new System.Windows.Forms.ComboBox();
            this.repeatVisitButton = new System.Windows.Forms.Button();
            this.unfocusLabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Пациент:";
            // 
            // patientBox
            // 
            this.patientBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.patientBox.Location = new System.Drawing.Point(81, 6);
            this.patientBox.Name = "patientBox";
            this.patientBox.ReadOnly = true;
            this.patientBox.Size = new System.Drawing.Size(301, 22);
            this.patientBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(17, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Доктор:";
            // 
            // doctorBox
            // 
            this.doctorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.doctorBox.Location = new System.Drawing.Point(81, 34);
            this.doctorBox.Name = "doctorBox";
            this.doctorBox.ReadOnly = true;
            this.doctorBox.Size = new System.Drawing.Size(301, 22);
            this.doctorBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(32, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Дата:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.doctorInfoButton);
            this.panel1.Controls.Add(this.dateBox);
            this.panel1.Controls.Add(this.patientInfoButton);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.patientBox);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.doctorBox);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(421, 95);
            this.panel1.TabIndex = 6;
            // 
            // doctorInfoButton
            // 
            this.doctorInfoButton.Image = global::Hospital.Properties.Resources.info_icon__2_;
            this.doctorInfoButton.Location = new System.Drawing.Point(388, 33);
            this.doctorInfoButton.Name = "doctorInfoButton";
            this.doctorInfoButton.Size = new System.Drawing.Size(25, 24);
            this.doctorInfoButton.TabIndex = 10;
            this.doctorInfoButton.UseVisualStyleBackColor = true;
            this.doctorInfoButton.Click += new System.EventHandler(this.doctorInfoButton_Click);
            this.doctorInfoButton.Enter += new System.EventHandler(this.unfocus);
            // 
            // dateBox
            // 
            this.dateBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateBox.Location = new System.Drawing.Point(81, 62);
            this.dateBox.Name = "dateBox";
            this.dateBox.ReadOnly = true;
            this.dateBox.Size = new System.Drawing.Size(301, 22);
            this.dateBox.TabIndex = 9;
            // 
            // patientInfoButton
            // 
            this.patientInfoButton.Image = global::Hospital.Properties.Resources.info_icon__2_;
            this.patientInfoButton.Location = new System.Drawing.Point(388, 5);
            this.patientInfoButton.Name = "patientInfoButton";
            this.patientInfoButton.Size = new System.Drawing.Size(25, 24);
            this.patientInfoButton.TabIndex = 7;
            this.patientInfoButton.UseVisualStyleBackColor = true;
            this.patientInfoButton.Click += new System.EventHandler(this.patientInfoButton_Click);
            this.patientInfoButton.Enter += new System.EventHandler(this.unfocus);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(13, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Диагноз:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(179, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Лечение:";
            // 
            // treatmentBox
            // 
            this.treatmentBox.Location = new System.Drawing.Point(3, 172);
            this.treatmentBox.Name = "treatmentBox";
            this.treatmentBox.Size = new System.Drawing.Size(421, 185);
            this.treatmentBox.TabIndex = 11;
            this.treatmentBox.Text = "";
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(268, 363);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 25);
            this.saveButton.TabIndex = 12;
            this.saveButton.Text = "Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            this.saveButton.Enter += new System.EventHandler(this.unfocus);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(349, 363);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 25);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            this.cancelButton.Enter += new System.EventHandler(this.unfocus);
            // 
            // diseaseBox
            // 
            this.diseaseBox.DropDownHeight = 100;
            this.diseaseBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.diseaseBox.FormattingEnabled = true;
            this.diseaseBox.IntegralHeight = false;
            this.diseaseBox.Location = new System.Drawing.Point(85, 117);
            this.diseaseBox.MaxDropDownItems = 6;
            this.diseaseBox.Name = "diseaseBox";
            this.diseaseBox.Size = new System.Drawing.Size(339, 24);
            this.diseaseBox.TabIndex = 14;
            this.diseaseBox.Enter += new System.EventHandler(this.diseaseBox_Enter);
            this.diseaseBox.Leave += new System.EventHandler(this.diseaseBox_Leave);
            // 
            // repeatVisitButton
            // 
            this.repeatVisitButton.Location = new System.Drawing.Point(4, 363);
            this.repeatVisitButton.Name = "repeatVisitButton";
            this.repeatVisitButton.Size = new System.Drawing.Size(109, 25);
            this.repeatVisitButton.TabIndex = 15;
            this.repeatVisitButton.Text = "Повторный приём";
            this.repeatVisitButton.UseVisualStyleBackColor = true;
            this.repeatVisitButton.Click += new System.EventHandler(this.repeatVisitButton_Click);
            this.repeatVisitButton.Enter += new System.EventHandler(this.unfocus);
            // 
            // unfocusLabel
            // 
            this.unfocusLabel.AutoSize = true;
            this.unfocusLabel.Location = new System.Drawing.Point(21, 153);
            this.unfocusLabel.Name = "unfocusLabel";
            this.unfocusLabel.Size = new System.Drawing.Size(0, 13);
            this.unfocusLabel.TabIndex = 16;
            // 
            // EditVisitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 396);
            this.Controls.Add(this.unfocusLabel);
            this.Controls.Add(this.repeatVisitButton);
            this.Controls.Add(this.diseaseBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.treatmentBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Name = "EditVisitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Информация";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditVisitForm_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox patientBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox doctorBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button patientInfoButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox treatmentBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox dateBox;
        private System.Windows.Forms.ComboBox diseaseBox;
        private System.Windows.Forms.Button repeatVisitButton;
        private System.Windows.Forms.Button doctorInfoButton;
        private System.Windows.Forms.Label unfocusLabel;
    }
}