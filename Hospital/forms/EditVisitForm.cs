﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hospital {
    public partial class EditVisitForm : Form {
        MainForm form;
        
        Doctor  doctor;
        Patient patient;
        Visit   visit;
        List<Disease> diseases;

        bool done = false;
        
        public EditVisitForm(MainForm form, Doctor doctor, Patient patient, Visit visit, List<Disease> diseases) {
            InitializeComponent();
            this.diseases = diseases;
            this.form = form;
            this.doctor = doctor;
            this.patient = patient;
            this.visit = visit;

            patientBox.Text = patient.Name;
            doctorBox.Text  = doctor.Name;
            dateBox.Text    = visit.Date;

            foreach (Disease disease in diseases) 
                diseaseBox.Items.Add(disease.Name);

            treatmentBox.Text = visit.Treatment;
            diseaseBox.SelectedIndex = diseaseBox.Items.IndexOf(visit.Disease);
        }

        private void saveButton_Click(object sender, EventArgs e) {
            diseaseBox_Leave(null, null);
            if (diseaseBox.BackColor != Color.White) {
                MessageBox.Show("Заболевание отсутствует в базе", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            done = true;
            visit.Disease = diseaseBox.Text.Trim();
            visit.Treatment = treatmentBox.Text.Trim();
            form.UpdateVisit(visit);
            form.UpdateVisitTab();
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            done = true;
            Close();
        }

        private void EditVisitForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (!done) {
                DialogResult result = MessageBox.Show("Сохранить изменения?", "Выход", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes) {
                    saveButton.PerformClick();
                } else if (result == DialogResult.Cancel) {
                    e.Cancel = true;
                }
            }
        }

        private void patientInfoButton_Click(object sender, EventArgs e) {
            new EntityInfoForm(patient).ShowDialog();
        }

        private void doctorInfoButton_Click(object sender, EventArgs e) {
            new EntityInfoForm(doctor).ShowDialog();
        }

        private void diseaseBox_Enter(object sender, EventArgs e) {
            diseaseBox.BackColor = Color.White;
        }

        private void diseaseBox_Leave(object sender, EventArgs e) {
            string text = diseaseBox.Text.Trim();
            foreach (Disease disease in diseases) {
                if (text == disease.Name)
                    return;
            }
            diseaseBox.BackColor = Color.LightSalmon;
        }

        private void repeatVisitButton_Click(object sender, EventArgs e) {
            List<Patient> patients = new List<Patient>{ patient };
            List<Doctor>  doctors  = new List<Doctor>{ doctor };

            new VisitForm(form, patients, doctors, VisitFormMode.Preselected).ShowDialog();
            form.UpdateVisitTab();
        }

        private void unfocus(object sender, EventArgs e) {
            unfocusLabel.Focus();
        }
    }
}
