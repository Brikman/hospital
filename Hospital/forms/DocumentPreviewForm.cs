﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hospital {
    public partial class DocumentPreviewForm : Form {
        public DocumentPreviewForm(string header, string text) {
            InitializeComponent();
            this.Text = header;
            textBox.Text = text;
        }

        private void printButton_Click(object sender, EventArgs e) {
            PrintDialog printDlg = new PrintDialog();

            PrintDocument printDoc = new PrintDocument();
            printDoc.DocumentName = this.Text;
            printDoc.PrintPage += printDoc_PrintPage;
            printDlg.Document = printDoc;

            if (printDlg.ShowDialog() == DialogResult.OK)
                printDoc.Print();
        }

        void printDoc_PrintPage(object sender, PrintPageEventArgs e) {
            e.Graphics.DrawString(this.textBox.Text, this.textBox.Font, Brushes.Black, 10, 25);
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            Close();
        }

        private void unfocus(object sender, EventArgs e) {
            textBox.Focus();
        }
    }
}
