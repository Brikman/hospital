﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hospital {
    public partial class SettingsForm : Form {
        private Settings settings;

        public SettingsForm(Settings settings) {
            InitializeComponent();
            this.settings = settings;

            InitSettings();
        }

        private void InitSettings() {
            InitSettingsTree();

            psqlPathTextBox.Text = settings.pg_home;
        }

        private void InitSettingsTree() {
            TreeNode node = new TreeNode();
            node.Name = "common";
            node.Text = "Common";

            treeView.Nodes.Add(node);
        }

        private void okButtonClick(object sender, EventArgs e) {
            UpdateSettings();
            Close();
        }

        private void UpdateSettings() {
            settings.pg_home = psqlPathTextBox.Text;
        }

        private void psqlPathTextBoxTextChanged(object sender, EventArgs e) {
            okButton.Enabled = true;
        }

        private void psqlBrowseButtonClick(object sender, EventArgs e) {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            DialogResult result = folderDialog.ShowDialog();
            if (result == DialogResult.OK) {
                psqlPathTextBox.Text = folderDialog.SelectedPath;
            }
        }

        private void cancelButtonClick(object sender, EventArgs e) {
            Close();
        }
    }
}
