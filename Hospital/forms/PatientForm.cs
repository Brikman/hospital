﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hospital {
    public partial class PatientForm : Form {
        private MainForm form;
        private bool done = false;
        private DateTime now;

        private int id = -1;
        private bool mode;

        private int minYear = 1900;

        public PatientForm(MainForm form) {
            InitializeComponent();
            this.form = form;

            now = DateTime.Now;
            for (int i = now.Year; i >= minYear; i--)
                yearBox.Items.Add(i);
            for (int i = 1; i <= 12; i++)
                monthBox.Items.Add(i);
            yearBox.SelectedIndex = yearBox.Items.IndexOf(2000);
            monthBox.SelectedIndex = 0;
        }

        public DialogResult AddPatient() {
            Text = "Новый пациент";
            mode = true;

            return ShowDialog();
        }

        public DialogResult EditPatient(Patient patient) {
            Text = "Пациент \"" + patient.Name + "\"";
            mode = false;

            id = patient.ID;
            nameBox.Text = patient.Name;
            maleRadioButton.Checked = patient.Sex;
            femaleRadioButton.Checked = !patient.Sex;
            phoneBox.Text = patient.Phone;
            string[] date = patient.BirthDate.Split('-');

            int day = Int32.Parse(date[0]);
            int month = Int32.Parse(date[1]);
            int year = Int32.Parse(date[2]);

            yearBox.SelectedIndex = now.Year - year;
            monthBox.SelectedIndex = month - 1;
            dayBox.SelectedIndex = day - 1;

            return ShowDialog();
        }

        private void saveButton_Click(object sender, EventArgs e) {
            string name = nameBox.Text.Trim();
            string date = (dayBox.SelectedIndex + 1) + "-" + (monthBox.SelectedIndex + 1) + "-" + (now.Year - yearBox.SelectedIndex);
            string phone = phoneBox.Text.Trim();
            bool sex = maleRadioButton.Checked;

            Patient patient = new Patient(id, name, sex, phone, date);

            if (mode)
                form.AddPatient(patient);
            else
                form.EditPatient(patient);

            done = true;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e) {
            done = true;
            Close();
        }

        private void EditForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (!done && nameBox.Text.Trim().Length > 0) {
                DialogResult result = MessageBox.Show("Сохранить изменения?", "Выход", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes) {
                    saveButton.PerformClick();
                } else if (result == DialogResult.Cancel) {
                    e.Cancel = true;
                }
            }
        }

        private void monthBox_SelectedIndexChanged(object sender, EventArgs e) {
            int month = monthBox.SelectedIndex + 1;
            int year = now.Year - yearBox.SelectedIndex;
            int days = (year == now.Year) ? now.Day : DateTime.DaysInMonth(year, month);

            dayBox.Items.Clear();
            for (int i = 1; i <= days; i++) 
                dayBox.Items.Add(i);

            dayBox.SelectedIndex = 0;
        }

        private void yearBox_SelectedIndexChanged(object sender, EventArgs e) {
            dayBox.Items.Clear();
            for (int i = 1; i <= 31; i++)
                dayBox.Items.Add(i);

            monthBox.Items.Clear();
            int year = now.Year - yearBox.SelectedIndex;
            int month = (year == now.Year) ? now.Month : 12;
            for (int i = 1; i <= month; i++)
                monthBox.Items.Add(i);
            monthBox.SelectedIndex = 0;
        }

        private void nameBox_TextChanged(object sender, EventArgs e) {
            saveButton.Enabled = nameBox.Text.Length > 0;
        }

        private void phoneBox_KeyPress(object sender, KeyPressEventArgs e) {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }  
    }
}
