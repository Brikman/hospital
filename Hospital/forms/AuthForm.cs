﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace Hospital {
    public partial class AuthForm : Form {
        MainForm form;
        bool success;

        public AuthForm(MainForm form) {
            InitializeComponent();
            this.form = form;
        }

        private void textChanged(object sender, EventArgs e) {
            loginButton.Enabled = (loginTextBox.Text.Length * passTextBox.Text.Length) > 0;
        }

        private void loginButton_Click(object sender, EventArgs e) {
            success = false;
            try {
                NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder();
                builder.Host = "localhost";
                builder.Port = 5432;
                builder.Username = loginTextBox.Text.Trim();
                builder.Password = passTextBox.Text.Trim();
                builder.Database = "hospital";

                form.InitConnection(builder);
                success = true;
                form.WindowState = FormWindowState.Normal;
                form.ShowInTaskbar = true;
                Close();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AuthForm_Load(object sender, EventArgs e) {
            //loginButton.PerformClick();
        }

        private void AuthForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (!success && !form.IsLogged()) 
                form.Close();
        }        
    }
}
