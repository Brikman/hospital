﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hospital {
    public partial class EntityInfoForm : Form {
        public EntityInfoForm(Entity entity) {
            InitializeComponent();

            Type type = entity.GetType();
            foreach (var f in type.GetFields().Where(f => f.IsPublic)) {
                int length = infoBox.Text.Length;
                string name = f.Name;
                string value = f.GetValue(entity).ToString();
                infoBox.AppendText(name + " :\t" + value + "\n");

                infoBox.Select(length, name.Length + 2);
                infoBox.SelectionFont = new Font(infoBox.Font, FontStyle.Bold);
            }

            infoBox.SelectAll();
            infoBox.SelectionTabs = new int[] { 0, 100 };
            infoBox.AcceptsTab = true;
            infoBox.Select(0, 0);
        }
    }
}
