﻿namespace Hospital {
    partial class MainForm {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.switchUserMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.settingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инструментыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.docsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.emptyStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.actionLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.visitPage = new System.Windows.Forms.TabPage();
            this.visitToolStrip = new System.Windows.Forms.ToolStrip();
            this.refreshVisitButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.denyVisitButton = new System.Windows.Forms.ToolStripButton();
            this.editVisitButton = new System.Windows.Forms.ToolStripButton();
            this.addVisitButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.columnVisitCombo = new System.Windows.Forms.ToolStripComboBox();
            this.searchVisitBox = new System.Windows.Forms.ToolStripTextBox();
            this.searchVisitButton = new System.Windows.Forms.ToolStripButton();
            this.prevVisitButton = new System.Windows.Forms.ToolStripButton();
            this.nextVisitButton = new System.Windows.Forms.ToolStripButton();
            this.cancelVisitSearchButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.visitInfoButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.diseaseButton = new System.Windows.Forms.ToolStripMenuItem();
            this.treatmentButton = new System.Windows.Forms.ToolStripMenuItem();
            this.visitGridView = new System.Windows.Forms.DataGridView();
            this.doctorPage = new System.Windows.Forms.TabPage();
            this.doctorToolStrip = new System.Windows.Forms.ToolStrip();
            this.refreshDoctorButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteDoctorButton = new System.Windows.Forms.ToolStripButton();
            this.editDoctorButton = new System.Windows.Forms.ToolStripButton();
            this.addDoctorButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.columnDoctorCombo = new System.Windows.Forms.ToolStripComboBox();
            this.searchDoctorBox = new System.Windows.Forms.ToolStripTextBox();
            this.searchDoctorButton = new System.Windows.Forms.ToolStripButton();
            this.prevDoctorButton = new System.Windows.Forms.ToolStripButton();
            this.nextDoctorButton = new System.Windows.Forms.ToolStripButton();
            this.cancelDoctorSearchButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.doctorGridView = new System.Windows.Forms.DataGridView();
            this.patientPage = new System.Windows.Forms.TabPage();
            this.patientToolStrip = new System.Windows.Forms.ToolStrip();
            this.refreshPatientButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.columnPatientCombo = new System.Windows.Forms.ToolStripComboBox();
            this.searchPatientBox = new System.Windows.Forms.ToolStripTextBox();
            this.searchPatientButton = new System.Windows.Forms.ToolStripButton();
            this.prevPatientButton = new System.Windows.Forms.ToolStripButton();
            this.nextPatientButton = new System.Windows.Forms.ToolStripButton();
            this.cancelPatientSearchButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.deletePatientButton = new System.Windows.Forms.ToolStripButton();
            this.editPatientButton = new System.Windows.Forms.ToolStripButton();
            this.addPatientButton = new System.Windows.Forms.ToolStripButton();
            this.patientGridView = new System.Windows.Forms.DataGridView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.visitPage.SuspendLayout();
            this.visitToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visitGridView)).BeginInit();
            this.doctorPage.SuspendLayout();
            this.doctorToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doctorGridView)).BeginInit();
            this.patientPage.SuspendLayout();
            this.patientToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientGridView)).BeginInit();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.инструментыToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(675, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userMenuItem,
            this.toolStripSeparator15,
            this.saveMenuItem,
            this.loadMenuItem,
            this.toolStripSeparator14,
            this.settingsMenuItem,
            this.toolStripSeparator13,
            this.exitMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // userMenuItem
            // 
            this.userMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.switchUserMenuItem});
            this.userMenuItem.Image = global::Hospital.Properties.Resources.user;
            this.userMenuItem.Name = "userMenuItem";
            this.userMenuItem.Size = new System.Drawing.Size(152, 22);
            this.userMenuItem.Text = "Пользователь";
            // 
            // switchUserMenuItem
            // 
            this.switchUserMenuItem.Image = global::Hospital.Properties.Resources._switch;
            this.switchUserMenuItem.Name = "switchUserMenuItem";
            this.switchUserMenuItem.Size = new System.Drawing.Size(152, 22);
            this.switchUserMenuItem.Text = "Сменить";
            this.switchUserMenuItem.Click += new System.EventHandler(this.switchUserMenuItem_Click);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(148, 6);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Image = global::Hospital.Properties.Resources.save;
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(151, 22);
            this.saveMenuItem.Text = "Сохранить";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // loadMenuItem
            // 
            this.loadMenuItem.Image = global::Hospital.Properties.Resources._24x24;
            this.loadMenuItem.Name = "loadMenuItem";
            this.loadMenuItem.Size = new System.Drawing.Size(151, 22);
            this.loadMenuItem.Text = "Восстановить";
            this.loadMenuItem.Click += new System.EventHandler(this.loadMenuItem_Click);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(148, 6);
            // 
            // settingsMenuItem
            // 
            this.settingsMenuItem.Image = global::Hospital.Properties.Resources.settings;
            this.settingsMenuItem.Name = "settingsMenuItem";
            this.settingsMenuItem.Size = new System.Drawing.Size(151, 22);
            this.settingsMenuItem.Text = "Настройки";
            this.settingsMenuItem.Click += new System.EventHandler(this.settingsMenuItem_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(148, 6);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Image = global::Hospital.Properties.Resources.exit;
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(151, 22);
            this.exitMenuItem.Text = "Выход";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // инструментыToolStripMenuItem
            // 
            this.инструментыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.docsToolStripMenuItem});
            this.инструментыToolStripMenuItem.Name = "инструментыToolStripMenuItem";
            this.инструментыToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.инструментыToolStripMenuItem.Text = "Инструменты";
            // 
            // docsToolStripMenuItem
            // 
            this.docsToolStripMenuItem.Image = global::Hospital.Properties.Resources.docs;
            this.docsToolStripMenuItem.Name = "docsToolStripMenuItem";
            this.docsToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.docsToolStripMenuItem.Text = "Справки";
            this.docsToolStripMenuItem.Click += new System.EventHandler(this.docsToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.emptyStatusLabel,
            this.actionLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 429);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(675, 22);
            this.statusStrip.TabIndex = 2;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // emptyStatusLabel
            // 
            this.emptyStatusLabel.Name = "emptyStatusLabel";
            this.emptyStatusLabel.Size = new System.Drawing.Size(660, 17);
            this.emptyStatusLabel.Spring = true;
            // 
            // actionLabel
            // 
            this.actionLabel.Name = "actionLabel";
            this.actionLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // visitPage
            // 
            this.visitPage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.visitPage.Controls.Add(this.visitToolStrip);
            this.visitPage.Controls.Add(this.visitGridView);
            this.visitPage.Location = new System.Drawing.Point(4, 29);
            this.visitPage.Name = "visitPage";
            this.visitPage.Size = new System.Drawing.Size(667, 366);
            this.visitPage.TabIndex = 2;
            this.visitPage.Text = "Посещения";
            // 
            // visitToolStrip
            // 
            this.visitToolStrip.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.visitToolStrip.AutoSize = false;
            this.visitToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.visitToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshVisitButton,
            this.toolStripSeparator9,
            this.toolStripSeparator10,
            this.denyVisitButton,
            this.editVisitButton,
            this.addVisitButton,
            this.toolStripLabel3,
            this.columnVisitCombo,
            this.searchVisitBox,
            this.searchVisitButton,
            this.prevVisitButton,
            this.nextVisitButton,
            this.cancelVisitSearchButton,
            this.toolStripSeparator11,
            this.toolStripSeparator12,
            this.visitInfoButton});
            this.visitToolStrip.Location = new System.Drawing.Point(3, 3);
            this.visitToolStrip.Name = "visitToolStrip";
            this.visitToolStrip.Padding = new System.Windows.Forms.Padding(0);
            this.visitToolStrip.Size = new System.Drawing.Size(661, 34);
            this.visitToolStrip.TabIndex = 3;
            // 
            // refreshVisitButton
            // 
            this.refreshVisitButton.AutoSize = false;
            this.refreshVisitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshVisitButton.Image = global::Hospital.Properties.Resources.refresh__1_;
            this.refreshVisitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshVisitButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.refreshVisitButton.Name = "refreshVisitButton";
            this.refreshVisitButton.Size = new System.Drawing.Size(30, 30);
            this.refreshVisitButton.Text = "Обновить список посещений";
            this.refreshVisitButton.Click += new System.EventHandler(this.refreshVisitButton_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 34);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 34);
            // 
            // denyVisitButton
            // 
            this.denyVisitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.denyVisitButton.AutoSize = false;
            this.denyVisitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.denyVisitButton.Enabled = false;
            this.denyVisitButton.ForeColor = System.Drawing.Color.Black;
            this.denyVisitButton.Image = global::Hospital.Properties.Resources.delete__1_;
            this.denyVisitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.denyVisitButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.denyVisitButton.Name = "denyVisitButton";
            this.denyVisitButton.Size = new System.Drawing.Size(30, 30);
            this.denyVisitButton.Text = "Отменить заявку";
            this.denyVisitButton.Click += new System.EventHandler(this.denyVisitButton_Click);
            // 
            // editVisitButton
            // 
            this.editVisitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.editVisitButton.AutoSize = false;
            this.editVisitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editVisitButton.Enabled = false;
            this.editVisitButton.ForeColor = System.Drawing.Color.Black;
            this.editVisitButton.Image = global::Hospital.Properties.Resources.edit__1_;
            this.editVisitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editVisitButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.editVisitButton.Name = "editVisitButton";
            this.editVisitButton.Size = new System.Drawing.Size(30, 30);
            this.editVisitButton.Text = "Принять пациента";
            this.editVisitButton.Click += new System.EventHandler(this.editVisitButton_Click);
            // 
            // addVisitButton
            // 
            this.addVisitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.addVisitButton.AutoSize = false;
            this.addVisitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addVisitButton.ForeColor = System.Drawing.Color.Black;
            this.addVisitButton.Image = global::Hospital.Properties.Resources._new;
            this.addVisitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addVisitButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.addVisitButton.Name = "addVisitButton";
            this.addVisitButton.Size = new System.Drawing.Size(30, 30);
            this.addVisitButton.Text = "Зарегистрировать заявку";
            this.addVisitButton.Click += new System.EventHandler(this.addVisitButton_Click);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(45, 31);
            this.toolStripLabel3.Text = "Поиск:";
            // 
            // columnVisitCombo
            // 
            this.columnVisitCombo.AutoSize = false;
            this.columnVisitCombo.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.columnVisitCombo.Items.AddRange(new object[] {
            "ID",
            "пациент",
            "доктор",
            "заболевание"});
            this.columnVisitCombo.Name = "columnVisitCombo";
            this.columnVisitCombo.Size = new System.Drawing.Size(100, 23);
            this.columnVisitCombo.TextChanged += new System.EventHandler(this.columnVisitCombo_TextChanged);
            // 
            // searchVisitBox
            // 
            this.searchVisitBox.AutoSize = false;
            this.searchVisitBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchVisitBox.Name = "searchVisitBox";
            this.searchVisitBox.Size = new System.Drawing.Size(120, 23);
            this.searchVisitBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchVisitBox_KeyDown);
            this.searchVisitBox.TextChanged += new System.EventHandler(this.searchVisitBox_TextChanged);
            // 
            // searchVisitButton
            // 
            this.searchVisitButton.AutoSize = false;
            this.searchVisitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchVisitButton.Enabled = false;
            this.searchVisitButton.Image = global::Hospital.Properties.Resources.search__1_;
            this.searchVisitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchVisitButton.Name = "searchVisitButton";
            this.searchVisitButton.Size = new System.Drawing.Size(30, 30);
            this.searchVisitButton.Text = "Поиск";
            this.searchVisitButton.Click += new System.EventHandler(this.searchVisitButton_Click);
            // 
            // prevVisitButton
            // 
            this.prevVisitButton.AutoSize = false;
            this.prevVisitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.prevVisitButton.Enabled = false;
            this.prevVisitButton.Image = global::Hospital.Properties.Resources.arrow_Previous_16xLG_color;
            this.prevVisitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.prevVisitButton.Name = "prevVisitButton";
            this.prevVisitButton.Size = new System.Drawing.Size(30, 30);
            this.prevVisitButton.Text = "Предыдущий результат";
            this.prevVisitButton.Click += new System.EventHandler(this.prevVisitButton_Click);
            // 
            // nextVisitButton
            // 
            this.nextVisitButton.AutoSize = false;
            this.nextVisitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nextVisitButton.Enabled = false;
            this.nextVisitButton.Image = global::Hospital.Properties.Resources.arrow_Next_16xLG_color;
            this.nextVisitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextVisitButton.Name = "nextVisitButton";
            this.nextVisitButton.Size = new System.Drawing.Size(30, 30);
            this.nextVisitButton.Text = "Следующий результат";
            this.nextVisitButton.Click += new System.EventHandler(this.nextVisitButton_Click);
            // 
            // cancelVisitSearchButton
            // 
            this.cancelVisitSearchButton.AutoSize = false;
            this.cancelVisitSearchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cancelVisitSearchButton.Enabled = false;
            this.cancelVisitSearchButton.Image = global::Hospital.Properties.Resources.action_Cancel_16xLG;
            this.cancelVisitSearchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancelVisitSearchButton.Name = "cancelVisitSearchButton";
            this.cancelVisitSearchButton.Size = new System.Drawing.Size(30, 30);
            this.cancelVisitSearchButton.Text = "Очистить поиск";
            this.cancelVisitSearchButton.Click += new System.EventHandler(this.cancelVisitSearchButton_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 34);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(6, 34);
            // 
            // visitInfoButton
            // 
            this.visitInfoButton.AutoSize = false;
            this.visitInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.visitInfoButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.diseaseButton,
            this.treatmentButton});
            this.visitInfoButton.Image = global::Hospital.Properties.Resources.info_icon__2_;
            this.visitInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.visitInfoButton.Margin = new System.Windows.Forms.Padding(4, 1, 0, 1);
            this.visitInfoButton.Name = "visitInfoButton";
            this.visitInfoButton.Size = new System.Drawing.Size(32, 30);
            this.visitInfoButton.Text = "Дополнительная информация";
            // 
            // diseaseButton
            // 
            this.diseaseButton.Name = "diseaseButton";
            this.diseaseButton.Size = new System.Drawing.Size(121, 22);
            this.diseaseButton.Text = "Диагноз";
            this.diseaseButton.Click += new System.EventHandler(this.diagnosisButton_Click);
            // 
            // treatmentButton
            // 
            this.treatmentButton.Name = "treatmentButton";
            this.treatmentButton.Size = new System.Drawing.Size(121, 22);
            this.treatmentButton.Text = "Лечение";
            this.treatmentButton.Click += new System.EventHandler(this.treatmentButton_Click);
            // 
            // visitGridView
            // 
            this.visitGridView.AllowUserToAddRows = false;
            this.visitGridView.AllowUserToDeleteRows = false;
            this.visitGridView.AllowUserToResizeRows = false;
            this.visitGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.visitGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.visitGridView.Location = new System.Drawing.Point(0, 38);
            this.visitGridView.MultiSelect = false;
            this.visitGridView.Name = "visitGridView";
            this.visitGridView.ReadOnly = true;
            this.visitGridView.Size = new System.Drawing.Size(667, 328);
            this.visitGridView.TabIndex = 0;
            this.visitGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.visitGridView_CellMouseClick);
            this.visitGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.visitGridView_CellMouseDoubleClick);
            this.visitGridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.visitGridView_DataBindingComplete);
            // 
            // doctorPage
            // 
            this.doctorPage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.doctorPage.Controls.Add(this.doctorToolStrip);
            this.doctorPage.Controls.Add(this.doctorGridView);
            this.doctorPage.Location = new System.Drawing.Point(4, 29);
            this.doctorPage.Name = "doctorPage";
            this.doctorPage.Padding = new System.Windows.Forms.Padding(3);
            this.doctorPage.Size = new System.Drawing.Size(667, 366);
            this.doctorPage.TabIndex = 1;
            this.doctorPage.Text = "Врачи";
            // 
            // doctorToolStrip
            // 
            this.doctorToolStrip.AutoSize = false;
            this.doctorToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshDoctorButton,
            this.toolStripSeparator5,
            this.toolStripSeparator6,
            this.deleteDoctorButton,
            this.editDoctorButton,
            this.addDoctorButton,
            this.toolStripLabel2,
            this.columnDoctorCombo,
            this.searchDoctorBox,
            this.searchDoctorButton,
            this.prevDoctorButton,
            this.nextDoctorButton,
            this.cancelDoctorSearchButton,
            this.toolStripSeparator7,
            this.toolStripSeparator8});
            this.doctorToolStrip.Location = new System.Drawing.Point(3, 3);
            this.doctorToolStrip.Name = "doctorToolStrip";
            this.doctorToolStrip.Padding = new System.Windows.Forms.Padding(0);
            this.doctorToolStrip.Size = new System.Drawing.Size(661, 34);
            this.doctorToolStrip.TabIndex = 1;
            // 
            // refreshDoctorButton
            // 
            this.refreshDoctorButton.AutoSize = false;
            this.refreshDoctorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshDoctorButton.Image = global::Hospital.Properties.Resources.refresh__1_;
            this.refreshDoctorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshDoctorButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.refreshDoctorButton.Name = "refreshDoctorButton";
            this.refreshDoctorButton.Size = new System.Drawing.Size(30, 30);
            this.refreshDoctorButton.Text = "Обновить список сотрудников";
            this.refreshDoctorButton.Click += new System.EventHandler(this.refreshDoctorButton_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 34);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 34);
            // 
            // deleteDoctorButton
            // 
            this.deleteDoctorButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.deleteDoctorButton.AutoSize = false;
            this.deleteDoctorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteDoctorButton.ForeColor = System.Drawing.Color.Black;
            this.deleteDoctorButton.Image = global::Hospital.Properties.Resources.delete__1_;
            this.deleteDoctorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteDoctorButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.deleteDoctorButton.Name = "deleteDoctorButton";
            this.deleteDoctorButton.Size = new System.Drawing.Size(30, 30);
            this.deleteDoctorButton.Text = "Удалить сотрудника из базы";
            this.deleteDoctorButton.Click += new System.EventHandler(this.deleteDoctorButton_Click);
            // 
            // editDoctorButton
            // 
            this.editDoctorButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.editDoctorButton.AutoSize = false;
            this.editDoctorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editDoctorButton.ForeColor = System.Drawing.Color.Black;
            this.editDoctorButton.Image = global::Hospital.Properties.Resources.edit__1_;
            this.editDoctorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editDoctorButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.editDoctorButton.Name = "editDoctorButton";
            this.editDoctorButton.Size = new System.Drawing.Size(30, 30);
            this.editDoctorButton.Text = "Редактировать информацию";
            this.editDoctorButton.Click += new System.EventHandler(this.editDoctorButton_Click);
            // 
            // addDoctorButton
            // 
            this.addDoctorButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.addDoctorButton.AutoSize = false;
            this.addDoctorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addDoctorButton.ForeColor = System.Drawing.Color.Black;
            this.addDoctorButton.Image = global::Hospital.Properties.Resources._new;
            this.addDoctorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addDoctorButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.addDoctorButton.Name = "addDoctorButton";
            this.addDoctorButton.Size = new System.Drawing.Size(30, 30);
            this.addDoctorButton.Text = "Зарегистрировать нового сотрудника";
            this.addDoctorButton.Click += new System.EventHandler(this.addDoctorButton_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(45, 31);
            this.toolStripLabel2.Text = "Поиск:";
            // 
            // columnDoctorCombo
            // 
            this.columnDoctorCombo.AutoSize = false;
            this.columnDoctorCombo.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.columnDoctorCombo.Items.AddRange(new object[] {
            "ID",
            "ФИО",
            "специальность",
            "кабинет"});
            this.columnDoctorCombo.Name = "columnDoctorCombo";
            this.columnDoctorCombo.Size = new System.Drawing.Size(100, 23);
            this.columnDoctorCombo.TextChanged += new System.EventHandler(this.columnSearchDoctorCombo_TextChanged);
            // 
            // searchDoctorBox
            // 
            this.searchDoctorBox.AutoSize = false;
            this.searchDoctorBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchDoctorBox.Name = "searchDoctorBox";
            this.searchDoctorBox.Size = new System.Drawing.Size(120, 23);
            this.searchDoctorBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchDoctorBox_KeyDown);
            this.searchDoctorBox.TextChanged += new System.EventHandler(this.searchDoctorBox_TextChanged);
            // 
            // searchDoctorButton
            // 
            this.searchDoctorButton.AutoSize = false;
            this.searchDoctorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchDoctorButton.Enabled = false;
            this.searchDoctorButton.Image = global::Hospital.Properties.Resources.search__1_;
            this.searchDoctorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchDoctorButton.Name = "searchDoctorButton";
            this.searchDoctorButton.Size = new System.Drawing.Size(30, 30);
            this.searchDoctorButton.Text = "Поиск";
            this.searchDoctorButton.Click += new System.EventHandler(this.searchDoctorButton_Click);
            // 
            // prevDoctorButton
            // 
            this.prevDoctorButton.AutoSize = false;
            this.prevDoctorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.prevDoctorButton.Enabled = false;
            this.prevDoctorButton.Image = global::Hospital.Properties.Resources.arrow_Previous_16xLG_color;
            this.prevDoctorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.prevDoctorButton.Name = "prevDoctorButton";
            this.prevDoctorButton.Size = new System.Drawing.Size(30, 30);
            this.prevDoctorButton.Text = "Предыдущий результат";
            this.prevDoctorButton.Click += new System.EventHandler(this.prevDoctorButton_Click);
            // 
            // nextDoctorButton
            // 
            this.nextDoctorButton.AutoSize = false;
            this.nextDoctorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nextDoctorButton.Enabled = false;
            this.nextDoctorButton.Image = global::Hospital.Properties.Resources.arrow_Next_16xLG_color;
            this.nextDoctorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextDoctorButton.Name = "nextDoctorButton";
            this.nextDoctorButton.Size = new System.Drawing.Size(30, 30);
            this.nextDoctorButton.Text = "Следующий результат";
            this.nextDoctorButton.Click += new System.EventHandler(this.nextDoctorButton_Click);
            // 
            // cancelDoctorSearchButton
            // 
            this.cancelDoctorSearchButton.AutoSize = false;
            this.cancelDoctorSearchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cancelDoctorSearchButton.Enabled = false;
            this.cancelDoctorSearchButton.Image = global::Hospital.Properties.Resources.action_Cancel_16xLG;
            this.cancelDoctorSearchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancelDoctorSearchButton.Name = "cancelDoctorSearchButton";
            this.cancelDoctorSearchButton.Size = new System.Drawing.Size(30, 30);
            this.cancelDoctorSearchButton.Text = "Очистить поиск";
            this.cancelDoctorSearchButton.Click += new System.EventHandler(this.cancelSearchDoctorButton_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 34);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 34);
            // 
            // doctorGridView
            // 
            this.doctorGridView.AllowUserToAddRows = false;
            this.doctorGridView.AllowUserToDeleteRows = false;
            this.doctorGridView.AllowUserToResizeRows = false;
            this.doctorGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.doctorGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.doctorGridView.Location = new System.Drawing.Point(0, 38);
            this.doctorGridView.Name = "doctorGridView";
            this.doctorGridView.ReadOnly = true;
            this.doctorGridView.Size = new System.Drawing.Size(667, 328);
            this.doctorGridView.TabIndex = 0;
            this.doctorGridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.doctorGridView_DataBindingComplete);
            this.doctorGridView.SelectionChanged += new System.EventHandler(this.doctorGridView_SelectionChanged);
            this.doctorGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.doctorGridView_KeyDown);
            // 
            // patientPage
            // 
            this.patientPage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.patientPage.Controls.Add(this.patientToolStrip);
            this.patientPage.Controls.Add(this.patientGridView);
            this.patientPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.patientPage.Location = new System.Drawing.Point(4, 29);
            this.patientPage.Name = "patientPage";
            this.patientPage.Padding = new System.Windows.Forms.Padding(3);
            this.patientPage.Size = new System.Drawing.Size(667, 366);
            this.patientPage.TabIndex = 0;
            this.patientPage.Text = "Пациенты";
            // 
            // patientToolStrip
            // 
            this.patientToolStrip.AutoSize = false;
            this.patientToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshPatientButton,
            this.toolStripSeparator1,
            this.toolStripSeparator3,
            this.toolStripLabel1,
            this.columnPatientCombo,
            this.searchPatientBox,
            this.searchPatientButton,
            this.prevPatientButton,
            this.nextPatientButton,
            this.cancelPatientSearchButton,
            this.toolStripSeparator2,
            this.toolStripSeparator4,
            this.deletePatientButton,
            this.editPatientButton,
            this.addPatientButton});
            this.patientToolStrip.Location = new System.Drawing.Point(3, 3);
            this.patientToolStrip.Name = "patientToolStrip";
            this.patientToolStrip.Padding = new System.Windows.Forms.Padding(0);
            this.patientToolStrip.Size = new System.Drawing.Size(661, 34);
            this.patientToolStrip.TabIndex = 1;
            this.patientToolStrip.Text = "toolStrip1";
            // 
            // refreshPatientButton
            // 
            this.refreshPatientButton.AutoSize = false;
            this.refreshPatientButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshPatientButton.Image = global::Hospital.Properties.Resources.refresh__1_;
            this.refreshPatientButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshPatientButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.refreshPatientButton.Name = "refreshPatientButton";
            this.refreshPatientButton.Size = new System.Drawing.Size(30, 30);
            this.refreshPatientButton.Text = "Обновить список пациентов";
            this.refreshPatientButton.Click += new System.EventHandler(this.refreshPatientButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 34);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 34);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(45, 31);
            this.toolStripLabel1.Text = "Поиск:";
            // 
            // columnPatientCombo
            // 
            this.columnPatientCombo.AutoSize = false;
            this.columnPatientCombo.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.columnPatientCombo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.columnPatientCombo.Items.AddRange(new object[] {
            "ID",
            "ФИО",
            "Дата рождения",
            "Пол"});
            this.columnPatientCombo.Name = "columnPatientCombo";
            this.columnPatientCombo.Size = new System.Drawing.Size(100, 23);
            this.columnPatientCombo.TextChanged += new System.EventHandler(this.columnSearchPatientCombo_TextChanged);
            // 
            // searchPatientBox
            // 
            this.searchPatientBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.searchPatientBox.Name = "searchPatientBox";
            this.searchPatientBox.Size = new System.Drawing.Size(120, 34);
            this.searchPatientBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.searchPatientBox_KeyDown);
            this.searchPatientBox.TextChanged += new System.EventHandler(this.searchPatientBox_TextChanged);
            // 
            // searchPatientButton
            // 
            this.searchPatientButton.AutoSize = false;
            this.searchPatientButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.searchPatientButton.Enabled = false;
            this.searchPatientButton.Image = global::Hospital.Properties.Resources.search__1_;
            this.searchPatientButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchPatientButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.searchPatientButton.Name = "searchPatientButton";
            this.searchPatientButton.Size = new System.Drawing.Size(30, 30);
            this.searchPatientButton.Text = "Поиск";
            this.searchPatientButton.Click += new System.EventHandler(this.searchPatientButton_Click);
            // 
            // prevPatientButton
            // 
            this.prevPatientButton.AutoSize = false;
            this.prevPatientButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.prevPatientButton.Enabled = false;
            this.prevPatientButton.Image = global::Hospital.Properties.Resources.arrow_Previous_16xLG_color;
            this.prevPatientButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.prevPatientButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.prevPatientButton.Name = "prevPatientButton";
            this.prevPatientButton.Size = new System.Drawing.Size(30, 30);
            this.prevPatientButton.Text = "Предыдущий результат";
            this.prevPatientButton.Click += new System.EventHandler(this.prevSearchPatientButton_Click);
            // 
            // nextPatientButton
            // 
            this.nextPatientButton.AutoSize = false;
            this.nextPatientButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.nextPatientButton.Enabled = false;
            this.nextPatientButton.Image = global::Hospital.Properties.Resources.arrow_Next_16xLG_color;
            this.nextPatientButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextPatientButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.nextPatientButton.Name = "nextPatientButton";
            this.nextPatientButton.Size = new System.Drawing.Size(30, 30);
            this.nextPatientButton.Text = "Следующий результат";
            this.nextPatientButton.Click += new System.EventHandler(this.nextSearchPatientButton_Click);
            // 
            // cancelPatientSearchButton
            // 
            this.cancelPatientSearchButton.AutoSize = false;
            this.cancelPatientSearchButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cancelPatientSearchButton.Enabled = false;
            this.cancelPatientSearchButton.Image = global::Hospital.Properties.Resources.action_Cancel_16xLG;
            this.cancelPatientSearchButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cancelPatientSearchButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.cancelPatientSearchButton.Name = "cancelPatientSearchButton";
            this.cancelPatientSearchButton.Size = new System.Drawing.Size(30, 30);
            this.cancelPatientSearchButton.Text = "Очистить поиск";
            this.cancelPatientSearchButton.Click += new System.EventHandler(this.cancelSearchButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 34);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 34);
            // 
            // deletePatientButton
            // 
            this.deletePatientButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.deletePatientButton.AutoSize = false;
            this.deletePatientButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deletePatientButton.Image = global::Hospital.Properties.Resources.delete__1_;
            this.deletePatientButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deletePatientButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.deletePatientButton.Name = "deletePatientButton";
            this.deletePatientButton.Size = new System.Drawing.Size(30, 30);
            this.deletePatientButton.Text = "Удалить пациента из базы";
            this.deletePatientButton.Click += new System.EventHandler(this.deletePatientButton_Click);
            // 
            // editPatientButton
            // 
            this.editPatientButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.editPatientButton.AutoSize = false;
            this.editPatientButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editPatientButton.Image = global::Hospital.Properties.Resources.edit__1_;
            this.editPatientButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editPatientButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.editPatientButton.Name = "editPatientButton";
            this.editPatientButton.Size = new System.Drawing.Size(30, 30);
            this.editPatientButton.Text = "Редактировать информацию";
            this.editPatientButton.Click += new System.EventHandler(this.editPatientButton_Click);
            // 
            // addPatientButton
            // 
            this.addPatientButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.addPatientButton.AutoSize = false;
            this.addPatientButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addPatientButton.Image = global::Hospital.Properties.Resources._new;
            this.addPatientButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addPatientButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 1);
            this.addPatientButton.Name = "addPatientButton";
            this.addPatientButton.Size = new System.Drawing.Size(30, 30);
            this.addPatientButton.Text = "Зарегистрировать нового пациента";
            this.addPatientButton.Click += new System.EventHandler(this.addPatientButton_Click);
            // 
            // patientGridView
            // 
            this.patientGridView.AllowUserToAddRows = false;
            this.patientGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.patientGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.patientGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.patientGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.patientGridView.Location = new System.Drawing.Point(0, 38);
            this.patientGridView.Name = "patientGridView";
            this.patientGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.patientGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.patientGridView.Size = new System.Drawing.Size(667, 328);
            this.patientGridView.TabIndex = 0;
            this.patientGridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.patientGridView_DataBindingComplete);
            this.patientGridView.SelectionChanged += new System.EventHandler(this.patientGridView_SelectionChanged);
            this.patientGridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.patientGridView_KeyDown);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.patientPage);
            this.tabControl.Controls.Add(this.doctorPage);
            this.tabControl.Controls.Add(this.visitPage);
            this.tabControl.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl.HotTrack = true;
            this.tabControl.ItemSize = new System.Drawing.Size(100, 25);
            this.tabControl.Location = new System.Drawing.Point(0, 27);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(675, 399);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 451);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.MinimumSize = new System.Drawing.Size(620, 460);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поликлиника";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainFormShown);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.visitPage.ResumeLayout(false);
            this.visitToolStrip.ResumeLayout(false);
            this.visitToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visitGridView)).EndInit();
            this.doctorPage.ResumeLayout(false);
            this.doctorToolStrip.ResumeLayout(false);
            this.doctorToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doctorGridView)).EndInit();
            this.patientPage.ResumeLayout(false);
            this.patientToolStrip.ResumeLayout(false);
            this.patientToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.patientGridView)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.TabPage visitPage;
        private System.Windows.Forms.ToolStrip visitToolStrip;
        private System.Windows.Forms.ToolStripButton refreshVisitButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton denyVisitButton;
        private System.Windows.Forms.ToolStripButton editVisitButton;
        private System.Windows.Forms.ToolStripButton addVisitButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox columnVisitCombo;
        private System.Windows.Forms.ToolStripTextBox searchVisitBox;
        private System.Windows.Forms.ToolStripButton searchVisitButton;
        private System.Windows.Forms.ToolStripButton prevVisitButton;
        private System.Windows.Forms.ToolStripButton nextVisitButton;
        private System.Windows.Forms.ToolStripButton cancelVisitSearchButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.DataGridView visitGridView;
        private System.Windows.Forms.TabPage doctorPage;
        private System.Windows.Forms.ToolStrip doctorToolStrip;
        private System.Windows.Forms.ToolStripButton refreshDoctorButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton deleteDoctorButton;
        private System.Windows.Forms.ToolStripButton editDoctorButton;
        private System.Windows.Forms.ToolStripButton addDoctorButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox columnDoctorCombo;
        private System.Windows.Forms.ToolStripTextBox searchDoctorBox;
        private System.Windows.Forms.ToolStripButton searchDoctorButton;
        private System.Windows.Forms.ToolStripButton prevDoctorButton;
        private System.Windows.Forms.ToolStripButton nextDoctorButton;
        private System.Windows.Forms.ToolStripButton cancelDoctorSearchButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.DataGridView doctorGridView;
        private System.Windows.Forms.TabPage patientPage;
        private System.Windows.Forms.ToolStrip patientToolStrip;
        private System.Windows.Forms.ToolStripButton refreshPatientButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox columnPatientCombo;
        private System.Windows.Forms.ToolStripTextBox searchPatientBox;
        private System.Windows.Forms.ToolStripButton searchPatientButton;
        private System.Windows.Forms.ToolStripButton prevPatientButton;
        private System.Windows.Forms.ToolStripButton nextPatientButton;
        private System.Windows.Forms.ToolStripButton cancelPatientSearchButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton deletePatientButton;
        private System.Windows.Forms.ToolStripButton editPatientButton;
        private System.Windows.Forms.ToolStripButton addPatientButton;
        private System.Windows.Forms.DataGridView patientGridView;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.ToolStripMenuItem инструментыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem docsToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton visitInfoButton;
        private System.Windows.Forms.ToolStripMenuItem diseaseButton;
        private System.Windows.Forms.ToolStripMenuItem treatmentButton;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripStatusLabel emptyStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel actionLabel;
        private System.Windows.Forms.ToolStripMenuItem userMenuItem;
        private System.Windows.Forms.ToolStripMenuItem switchUserMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripMenuItem settingsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
    }
}

