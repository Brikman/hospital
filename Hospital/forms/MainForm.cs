﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Npgsql;

namespace Hospital {
    public partial class MainForm : Form {
        NpgsqlConnection conn;
        NpgsqlDataAdapter adapter;

        Searcher patientSearcher = new Searcher();
        Searcher doctorSearcher  = new Searcher();
        Searcher visitSearcher   = new Searcher();

        Settings settings = new Settings();
        string password;

        public MainForm() {
            InitializeComponent();
            WindowState = FormWindowState.Minimized;
            ShowInTaskbar = false;
        }

        private void MainFormShown(object sender, EventArgs e) {
            new AuthForm(this).ShowDialog();
        }

        public void InitConnection(NpgsqlConnectionStringBuilder builder) {
            conn = new NpgsqlConnection(builder.ToString());
            conn.Open();
            conn.Close();

            password = builder.Password;

            UpdatePatientTab();
            UpdateDoctorTab();
            UpdateVisitTab();
        }

        public void UpdatePatientTab() {
            try {
                string query = "select * from patients_view";
                adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();

                conn.Open();
                adapter.Fill(set);

                set.Tables[0].Columns[0].ColumnName = "ID";
                set.Tables[0].Columns[1].ColumnName = "ФИО";
                set.Tables[0].Columns[2].ColumnName = "Пол";
                set.Tables[0].Columns[3].ColumnName = "Дата рождения";
                set.Tables[0].Columns[4].ColumnName = "Телефон";

                patientGridView.DataSource = set.Tables[0];
            } catch (Exception ex) {
                ShowError(ex.Message);
            } finally {
                conn.Close();
            }
            UpdateStatus();
        }

        public void UpdateDoctorTab() {
            try {
                string query = "select * from doctors_view";
                adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();

                conn.Open();
                adapter.Fill(set);

                set.Tables[0].Columns[0].ColumnName = "ID";
                set.Tables[0].Columns[1].ColumnName = "ФИО";
                set.Tables[0].Columns[2].ColumnName = "Кабинет";
                set.Tables[0].Columns[3].ColumnName = "Специальность";
                set.Tables[0].Columns[4].ColumnName = "Принят на работу";

                doctorGridView.DataSource = set.Tables[0];
            } catch (Exception ex) {
                ShowError(ex.Message);
            } finally {
                conn.Close();
            }
            UpdateStatus();
        }

        public void UpdateVisitTab() {
            try {
                string query = "select * from visits_view";
                adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();

                conn.Open();
                adapter.Fill(set);

                set.Tables[0].Columns[0].ColumnName = "ID";
                set.Tables[0].Columns[1].ColumnName = "Пациент";
                set.Tables[0].Columns[2].ColumnName = "Доктор";
                set.Tables[0].Columns[3].ColumnName = "Дата приёма";
                set.Tables[0].Columns[4].ColumnName = "Статус";
                set.Tables[0].Columns[5].ColumnName = "Диагноз";
                set.Tables[0].Columns[6].ColumnName = "Лечение";

                visitGridView.DataSource = set.Tables[0];
            } catch (Exception ex) {
                ShowError(ex.Message);
            } finally {
                conn.Close();
            }
            UpdateStatus();
        }

        public void AddPatient(Patient patient) {
            NpgsqlTransaction trans = null;
            try {
                string query = String.Format("insert into patients(name, sex, birth_date, phone_num) " + 
                                             "values('{0}', {1}, '{2}', '{3}') returning id",
                                              patient.Name, patient.Sex, patient.BirthDate, patient.Phone);
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                trans = conn.BeginTransaction();
                int id = (int)command.ExecuteScalar();
                trans.Commit();
                ShowInfo("Пациент зарегистрирован под номером ID: " + id);
                UpdateAction("Пациент зарегистрирован (ID: " + id + ")");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        public void EditPatient(Patient patient) {
            NpgsqlTransaction trans = null;
            try {
                string query = String.Format(
                    "update patients set name='{0}', sex={1}, birth_date='{2}', phone_num={3} where id={4}",
                    patient.Name, patient.Sex, patient.BirthDate, patient.Phone, patient.ID);
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                trans = conn.BeginTransaction();
                command.ExecuteNonQuery();
                trans.Commit();
                ShowInfo("Изменения для пациента ID: " + patient.ID + " сохранены");
                UpdateAction("Изменения для пациента ID: " + patient.ID + " сохранены");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        public void AddDoctor(Doctor doctor) {
            NpgsqlTransaction trans = null;
            try {
                string query;
                query = "select id from specs where name='" + doctor.Spec + "'";
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                int id_spec = (int)command.ExecuteScalar();

                query = String.Format(
                    "insert into doctors(name, id_spec, room, empl_date) " + 
                    " values('{0}', {1}, {2}, '{3}') returning id",
                    doctor.Name, id_spec, doctor.Room, doctor.Date);
                command.CommandText = query;
                trans = conn.BeginTransaction();
                int id = (int)command.ExecuteScalar();
                trans.Commit();
                ShowInfo("Сотрудник зарегистрирован под номером ID: " + id);
                UpdateAction("Сотрудник зарегистрирован (ID: " + id + ")");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        public void EditDoctor(Doctor doctor) {
            NpgsqlTransaction trans = null;
            try {
                string query;
                query = "select id from specs where name='" + doctor.Spec + "'";
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                int id_spec = (int)command.ExecuteScalar();

                query = String.Format(
                    "update doctors set name='{0}', id_spec={1}, room={2}, empl_date='{3}' where id={4}",
                    doctor.Name, id_spec, doctor.Room, doctor.Date, doctor.ID);
                command.CommandText = query;
                trans = conn.BeginTransaction();
                command.ExecuteScalar();
                trans.Commit();
                ShowInfo("Изменения для сотрудника ID: " + doctor.ID + " сохранены");
                UpdateAction("Информация сотрудника изменена (ID: " + doctor.ID + ")");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        public void DeletePatient(int id) {
            NpgsqlTransaction trans = null;
            try {
                string query = "delete from patients where id=" + id;
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                trans = conn.BeginTransaction();
                command.ExecuteNonQuery();
                trans.Commit();
                ShowInfo("Пациент ID: " + id + " удалён из базы");
                UpdateAction("Пациент удалён (ID: " + id + ")");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        public void DeleteDoctor(int id) {
            NpgsqlTransaction trans = null;
            try {
                string query = "delete from doctors where id=" + id;
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                trans = conn.BeginTransaction();
                command.ExecuteNonQuery();
                trans.Commit();
                ShowInfo("Сотрудник ID: " + id + " удален из базы");
                UpdateAction("Сотрудник удален (ID: " + id + ")");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        public void AddVisit(Visit visit) {
            int id = visit.ID;
            int id_patient = visit.IDPatient;
            int id_doctor  = visit.IDDoctor;
            string date    = visit.Date;

            NpgsqlTransaction trans = null;
            try {
                string query = String.Format(
                    "insert into visits(id_patient, id_doctor, id_disease, date, status) " +
                    "values({0}, {1}, {2}, '{3}', {4}) returning id",
                    id_patient, id_doctor, -1, date, 0
                );
                Console.WriteLine(query);
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                trans = conn.BeginTransaction();
                int id_visit = (int)command.ExecuteScalar();
                trans.Commit();
                ShowInfo("Заявка зарегистрирована под номером ID: " + id_visit);
                UpdateAction("Заявка зарегистрирована (ID: " + id_visit + ")");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        public void UpdateVisit(Visit visit) {
            NpgsqlTransaction trans = null;
            try {
                string query = String.Format(
                    "update visits set id_disease=(select id from diseases where name='{0}'), " + 
                    "status={1}, comment='{2}' where id={3}",
                    visit.Disease, 1, visit.Treatment, visit.ID);
                Console.WriteLine(query);
                NpgsqlCommand command = conn.CreateCommand();
                command.CommandText = query;
                conn.Open();
                trans = conn.BeginTransaction();
                command.ExecuteNonQuery();
                trans.Commit();
                ShowInfo("Заявка под номером ID: " + visit.ID + " обновлена. Пациент принят");
            } catch (Exception ex) {
                ShowError(ex.Message);
                if (trans != null)
                    trans.Rollback();
            } finally {
                conn.Close();
            }
        }

        private void ShowError(string error, string title = "Error") {
            MessageBox.Show(error, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ShowInfo(string info, string title = "Info") {
            MessageBox.Show(info, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void refreshPatientButton_Click(object sender, EventArgs e) {
            UpdatePatientTab();
        }

        private void addPatientButton_Click(object sender, EventArgs e) {
            new PatientForm(this).AddPatient();
            UpdatePatientTab();
        }

        private void editPatientButton_Click(object sender, EventArgs e) {
            DataGridViewCellCollection cells = patientGridView.SelectedRows[0].Cells;
            int id = (int)cells[0].Value;
            string name = cells[1].Value.ToString().Trim();
            bool sex = cells[2].Value.ToString().ToUpper().Equals("М");
            DateTime dt = (DateTime)cells[3].Value;
            string date = dt.Day + "-" + dt.Month + "-" + dt.Year;
            string phone = cells[4].Value.ToString().Trim();

            Patient patient = new Patient(id, name, sex, phone, date);
            new PatientForm(this).EditPatient(patient);
            UpdatePatientTab();
        }

        private void patientGridView_SelectionChanged(object sender, EventArgs e) {
            bool enabled = patientGridView.SelectedRows.Count > 0;
            editPatientButton.Enabled = enabled;
            deletePatientButton.Enabled = enabled;
        }

        private void deletePatientButton_Click(object sender, EventArgs e) {
            int id = (int)patientGridView.SelectedRows[0].Cells[0].Value;
            DialogResult result = MessageBox.Show("Удалить пациента ID: " + id + "?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes) {
                DeletePatient(id);
                UpdatePatientTab();
            }
        }

        private void patientGridView_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyData == Keys.Delete) {
                DeleteSelectedPatients();
            }
        }

        private void DeleteSelectedPatients() {
            int amountDeleted = patientGridView.SelectedRows.Count;
            if (amountDeleted > 0) {
                DialogResult dialogResult = MessageBox.Show(
                    "Удалить выбранных пациентов (" + patientGridView.SelectedRows.Count + ")?",
                    "Удаление",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question
                );
                if (dialogResult == DialogResult.Yes) {
                    foreach (DataGridViewRow row in patientGridView.SelectedRows) {
                        int id = (int)row.Cells[0].Value;
                        DeletePatient(id);
                    }
                }
                patientGridView.ClearSelection();
                UpdatePatientTab();
            }
        }

        private void DeleteSelectedDoctors() {
            int amountDeleted = doctorGridView.SelectedRows.Count;
            if (amountDeleted > 0) {
                DialogResult dialogResult = MessageBox.Show(
                    "Удалить выбранных сотрудников (" + doctorGridView.SelectedRows.Count + ")?",
                    "Удаление",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question
                );
                if (dialogResult == DialogResult.Yes) {
                    foreach (DataGridViewRow row in doctorGridView.SelectedRows) {
                        int id = (int)row.Cells[0].Value;
                        DeleteDoctor(id);
                    }
                }
                patientGridView.ClearSelection();
                UpdateDoctorTab();
            }
        }

        private void searchPatientBox_TextChanged(object sender, EventArgs e) {
            bool enabled = searchPatientBox.Text.Length * columnPatientCombo.Text.Length > 0;
            searchPatientButton.Enabled = enabled;
            nextPatientButton.Enabled = enabled;
            prevPatientButton.Enabled = enabled;
            cancelPatientSearchButton.Enabled = enabled;
        }

        private void columnSearchPatientCombo_TextChanged(object sender, EventArgs e) {
            bool enabled = searchPatientBox.Text.Length * columnPatientCombo.Text.Length > 0;
            searchPatientButton.Enabled = enabled;
            nextPatientButton.Enabled = enabled;
            prevPatientButton.Enabled = enabled;
            cancelPatientSearchButton.Enabled = enabled;
        }

        private void searchPatientButton_Click(object sender, EventArgs e) {
            string column = columnPatientCombo.Text.Trim();
            string search = searchPatientBox.Text.Trim().ToLower();

            patientGridView.ClearSelection();
            patientSearcher.Begin(patientGridView, column, search);
            nextPatientButton.PerformClick();
        }

        private void refreshDoctorButton_Click(object sender, EventArgs e) {
            UpdateDoctorTab();
        }

        private void addDoctorButton_Click(object sender, EventArgs e) {
            new DoctorForm(this, conn).AddDoctor();
            UpdateDoctorTab();
        }

        private void editDoctorButton_Click(object sender, EventArgs e) {
            DataGridViewCellCollection cells = doctorGridView.SelectedRows[0].Cells;
            int id = (int)cells[0].Value;
            string name = cells[1].Value.ToString().Trim();
            int room = (int)cells[2].Value;
            string spec = cells[3].Value.ToString().Trim();
            DateTime dt = (DateTime)cells[4].Value;
            string date = dt.Day + "-" + dt.Month + "-" + dt.Year;
            string phone = cells[4].Value.ToString().Trim();

            Doctor doctor = new Doctor(id, name, spec, room, date);
            new DoctorForm(this, conn).EditDoctor(doctor);
            UpdateDoctorTab();
        }

        private void doctorGridView_SelectionChanged(object sender, EventArgs e) {
            bool enabled = doctorGridView.SelectedRows.Count > 0;
            editDoctorButton.Enabled = enabled;
            deleteDoctorButton.Enabled = enabled;
        }

        private void doctorGridView_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyData == Keys.Delete) {
                DeleteSelectedDoctors();
            }
        }

        private void deleteDoctorButton_Click(object sender, EventArgs e) {
            int id = (int)doctorGridView.SelectedRows[0].Cells[0].Value;
            DialogResult result = MessageBox.Show("Удалить сотрудника ID: " + id + "?", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes) {
                DeleteDoctor(id);
                UpdateDoctorTab();
            }
        }

        private void nextSearchPatientButton_Click(object sender, EventArgs e) {
            DataGridViewCell cell = patientSearcher.FindNext();
            if (cell != null) {
                patientGridView.ClearSelection();
                cell.Selected = true;

                patientGridView.CurrentCell = cell;
            } else {
                ShowInfo("Поиск завершен");
            }
        }

        private void prevSearchPatientButton_Click(object sender, EventArgs e) {
            DataGridViewCell cell = patientSearcher.FindPrev();
            if (cell != null) {
                patientGridView.ClearSelection();
                cell.Selected = true;

                patientGridView.CurrentCell = cell;
            } else {
                ShowInfo("Поиск завершен");
            }
        }

        private void cancelSearchButton_Click(object sender, EventArgs e) {
            columnPatientCombo.Text = "";
            searchPatientBox.Text = "";
            searchPatientButton.Enabled = false;
            nextPatientButton.Enabled = false;
            prevPatientButton.Enabled = false;
            cancelPatientSearchButton.Enabled = false;
            patientGridView.ClearSelection();

            patientSearcher.Terminate();
        }

        private void searchPatientBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyData == Keys.Enter && searchPatientButton.Enabled) {
                searchPatientButton.PerformClick();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (conn != null && conn.State != ConnectionState.Closed)
                conn.Close();

            patientSearcher.Terminate();
            doctorSearcher.Terminate();

        }

        private void searchDoctorButton_Click(object sender, EventArgs e) {
            string column = columnDoctorCombo.Text.Trim();
            string search = searchDoctorBox.Text.Trim().ToLower();

            doctorGridView.ClearSelection();
            doctorSearcher.Begin(doctorGridView, column, search);
            nextDoctorButton.PerformClick();
        }

        private void prevDoctorButton_Click(object sender, EventArgs e) {
            DataGridViewCell cell = doctorSearcher.FindPrev();
            if (cell != null) {
                doctorGridView.ClearSelection();
                cell.Selected = true;

                doctorGridView.CurrentCell = cell;
            } else {
                ShowInfo("Поиск завершен");
            }
        }

        private void nextDoctorButton_Click(object sender, EventArgs e) {
            DataGridViewCell cell = doctorSearcher.FindNext();
            if (cell != null) {
                doctorGridView.ClearSelection();
                cell.Selected = true;

                doctorGridView.CurrentCell = cell;
            } else {
                ShowInfo("Поиск завершен");
            }
        }

        private void cancelSearchDoctorButton_Click(object sender, EventArgs e) {
            columnDoctorCombo.Text = "";
            searchDoctorBox.Text = "";
            searchDoctorButton.Enabled = false;
            nextDoctorButton.Enabled = false;
            prevDoctorButton.Enabled = false;
            cancelDoctorSearchButton.Enabled = false;
            doctorGridView.ClearSelection();

            doctorSearcher.Terminate();
        }

        private void columnSearchDoctorCombo_TextChanged(object sender, EventArgs e) {
            bool enabled = searchDoctorBox.Text.Length * columnDoctorCombo.Text.Length > 0;
            searchDoctorButton.Enabled = enabled;
            nextDoctorButton.Enabled = enabled;
            prevDoctorButton.Enabled = enabled;
            cancelDoctorSearchButton.Enabled = enabled;
        }

        private void searchDoctorBox_TextChanged(object sender, EventArgs e) {
            bool enabled = searchDoctorBox.Text.Length * columnDoctorCombo.Text.Length > 0;
            searchDoctorButton.Enabled = enabled;
            nextDoctorButton.Enabled = enabled;
            prevDoctorButton.Enabled = enabled;
            cancelDoctorSearchButton.Enabled = enabled;
        }

        private void searchDoctorBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyData == Keys.Enter && searchDoctorButton.Enabled) {
                searchDoctorButton.PerformClick();
            }
        }

        private void searchVisitButton_Click(object sender, EventArgs e) {
            string column = columnVisitCombo.Text.Trim();
            string search = searchVisitBox.Text.Trim().ToLower();

            visitGridView.ClearSelection();
            visitSearcher.Begin(visitGridView, column, search);
            nextVisitButton.PerformClick();
        }

        private void prevVisitButton_Click(object sender, EventArgs e) {
            DataGridViewCell cell = visitSearcher.FindPrev();
            if (cell != null) {
                visitGridView.ClearSelection();
                cell.Selected = true;

                visitGridView.CurrentCell = cell;
            } else {
                ShowInfo("Поиск завершен");
            }
        }

        private void nextVisitButton_Click(object sender, EventArgs e) {
            DataGridViewCell cell = visitSearcher.FindNext();
            if (cell != null) {
                visitGridView.ClearSelection();
                cell.Selected = true;

                visitGridView.CurrentCell = cell;
            } else {
                ShowInfo("Поиск завершен");
            }
        }

        private void cancelVisitSearchButton_Click(object sender, EventArgs e) {
            columnVisitCombo.Text = "";
            searchVisitBox.Text = "";
            searchVisitButton.Enabled = false;
            nextVisitButton.Enabled = false;
            prevVisitButton.Enabled = false;
            cancelVisitSearchButton.Enabled = false;
            visitGridView.ClearSelection();

            visitSearcher.Terminate();
        }

        private void columnVisitCombo_TextChanged(object sender, EventArgs e) {
            bool enabled = searchVisitBox.Text.Length * columnVisitCombo.Text.Length > 0;
            searchVisitButton.Enabled = enabled;
            nextVisitButton.Enabled = enabled;
            prevVisitButton.Enabled = enabled;
            cancelVisitSearchButton.Enabled = enabled;
        }

        private void searchVisitBox_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyData == Keys.Enter && searchVisitButton.Enabled) {
                searchVisitButton.PerformClick();
            }
        }

        private void searchVisitBox_TextChanged(object sender, EventArgs e) {
            bool enabled = searchVisitBox.Text.Length * columnVisitCombo.Text.Length > 0;
            searchVisitButton.Enabled = enabled;
            nextVisitButton.Enabled = enabled;
            prevVisitButton.Enabled = enabled;
            cancelVisitSearchButton.Enabled = enabled;
        }

        private void refreshVisitButton_Click(object sender, EventArgs e) {
            UpdateVisitTab();
        }

        private void patientGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            patientGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            patientGridView.EnableHeadersVisualStyles = false;

            patientGridView.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            patientGridView.Columns["Пол"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            foreach (DataGridViewColumn column in patientGridView.Columns)
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            patientGridView.Columns["Пол"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            patientGridView.Columns["Дата рождения"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            patientGridView.Columns["Телефон"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void doctorGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            doctorGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            doctorGridView.EnableHeadersVisualStyles = false;

            doctorGridView.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            doctorGridView.Columns["Кабинет"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;

            foreach (DataGridViewColumn column in doctorGridView.Columns)
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            doctorGridView.Columns["Кабинет"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            doctorGridView.Columns["Принят на работу"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }

        private void visitGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e) {
            visitGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            visitGridView.Columns["Диагноз"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            visitGridView.Columns["Диагноз"].Visible = diseaseButton.Image != null;
            visitGridView.Columns["Лечение"].Visible = treatmentButton.Image != null;
            visitGridView.EnableHeadersVisualStyles = false;

            foreach (DataGridViewColumn column in visitGridView.Columns)
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            visitGridView.Columns["Статус"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            foreach (DataGridViewRow row in visitGridView.Rows) {
                DataGridViewCell cell  = row.Cells["Статус"];
                string status = ((string)cell.Value).Trim();
                if (status == "принят")
                    cell.Style.BackColor = Color.LightGreen;
                else if (status == "отменён") 
                    cell.Style.BackColor = Color.LightSalmon;
                else 
                    cell.Style.BackColor = Color.LightGray;

                cell = row.Cells["Диагноз"];
                if (cell.Value != DBNull.Value && ((string)cell.Value).Trim().ToLower() == "не определён")
                    cell.Style.BackColor = Color.Yellow;
            }
            
            visitGridView.Columns["ID"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            visitGridView.Columns["Дата приёма"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            visitGridView.Columns["Статус"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        private void diagnosisButton_Click(object sender, EventArgs e) {
            bool visible = visitGridView.Columns["Диагноз"].Visible;
            visitGridView.Columns["Диагноз"].Visible = !visible;
            diseaseButton.Image = (!visible) ? Hospital.Properties.Resources.check__1_ : null;
        }

        private void treatmentButton_Click(object sender, EventArgs e) {
            bool visible = visitGridView.Columns["Лечение"].Visible;
            visitGridView.Columns["Лечение"].Visible = !visible;
            treatmentButton.Image = (!visible) ? Hospital.Properties.Resources.check__1_ : null;
        }

        private void docsToolStripMenuItem_Click(object sender, EventArgs e) {
            new DocumentForm(conn).ShowDialog();
        }

        private void addVisitButton_Click(object sender, EventArgs e) {
            List<Patient> patients = new List<Patient>();
            List<Doctor>  doctors  = new List<Doctor>();

            foreach (DataGridViewRow row in patientGridView.Rows) {
                int    id = (int)row.Cells[0].Value;
                string name = ((string)row.Cells[1].Value).Trim();
                bool   sex = ((string)row.Cells[2].Value).Trim().ToUpper() == "М";
                string birthDate = ((DateTime)row.Cells[3].Value).ToShortDateString();
                string phone = ((string)row.Cells[4].Value).Trim();

                patients.Add(new Patient(id, name, sex, phone, birthDate));
            }

            foreach (DataGridViewRow row in doctorGridView.Rows) {
                int    id = (int)row.Cells[0].Value;
                string name = ((string)row.Cells[1].Value).Trim();
                int    room = (int)row.Cells[2].Value;
                string spec = ((string)row.Cells[3].Value).Trim();
                string date = ((DateTime)row.Cells[4].Value).ToShortDateString();

                doctors.Add(new Doctor(id, name, spec, room, date));
            }

            new VisitForm(this, patients, doctors).ShowDialog();
        }

        private void visitGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e) {
            Console.WriteLine("CLICK");
            if (visitGridView.SelectedCells.Count > 0) {
                DataGridViewRow row = visitGridView.SelectedCells[0].OwningRow;
                bool enabled = ((string)row.Cells[4].Value).Trim() == "ожидает";
                denyVisitButton.Enabled = enabled;
                editVisitButton.Enabled = enabled;
            }
        }

        private void visitGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e) {
            editVisitButton.PerformClick();
        }

        private void editVisitButton_Click(object sender, EventArgs e) {
            if (!editVisitButton.Enabled)
                return;
            DataGridViewRow row = visitGridView.Rows[visitGridView.SelectedCells[0].RowIndex];
            DataSet set = new DataSet();

            int id_visit = -1;
            int id_patient = -1;
            int id_doctor = -1;
            try {
                id_visit = (int)row.Cells[0].Value;
                string query = "SELECT id, name from diseases where id=0 or id_spec=" +
                               "(select id_spec from doctors where id=" +
                               "(select id_doctor from visits where id=" + id_visit + "))";
                NpgsqlCommand command = new NpgsqlCommand(query, conn);
                adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = command;
                conn.Open();
                adapter.Fill(set);
                //**********************************************************
                query = "select id_patient from visits where id=" + id_visit;
                command = new NpgsqlCommand(query, conn);
                id_patient = (int)command.ExecuteScalar();
                //**********************************************************
                query = "select id_doctor from visits where id=" + id_visit;
                command = new NpgsqlCommand(query, conn);
                id_doctor = (int)command.ExecuteScalar();
            } catch (Exception ex) {
                ShowError(ex.Message);
            } finally {
                conn.Close();
            }

            List<Disease> diseases = new List<Disease>();
            foreach (DataRow dataRow in set.Tables[0].Rows) {
                int id = (int)dataRow.ItemArray[0];
                if (id < 0)
                    continue;
                string name = ((string)dataRow.ItemArray[1]).Trim();
                diseases.Add(new Disease(id, name));
            }

            Patient patient = null;
            foreach (DataGridViewRow r in patientGridView.Rows) {
                if ((int)r.Cells[0].Value == id_patient) {
                    string name = ((string)r.Cells[1].Value).Trim();
                    bool sex = ((string)r.Cells[2].Value).Trim().ToUpper() == "М";
                    string date = ((DateTime)r.Cells[3].Value).ToShortDateString();
                    string phone = ((string)r.Cells[4].Value).Trim();

                    patient = new Patient(id_patient, name, sex, phone, date);
                    break;
                }
            }

            Doctor doctor = null;
            foreach (DataGridViewRow r in doctorGridView.Rows) {
                if ((int)r.Cells[0].Value == id_doctor) {
                    string name = ((string)r.Cells[1].Value).Trim();
                    int room = (int)r.Cells[2].Value;
                    string spec = ((string)r.Cells[3].Value).Trim();
                    string date = ((DateTime)r.Cells[4].Value).ToShortDateString();

                    doctor = new Doctor(id_doctor, name, spec, room, date);
                    break;
                }
            }

            Visit visit = new Visit(id_visit, id_patient, id_doctor, ((DateTime)row.Cells[3].Value).ToString());
            if (row.Cells[6].Value != DBNull.Value)
                visit.Treatment = (string)row.Cells[6].Value;
            else
                visit.Treatment = "";

            if (row.Cells[5].Value != DBNull.Value)
                visit.Disease = ((string)row.Cells[5].Value).Trim();
            else
                visit.Disease = "";

            new EditVisitForm(this, doctor, patient, visit, diseases).ShowDialog();
        }

        private void denyVisitButton_Click(object sender, EventArgs e) {
            int id = (int)visitGridView.SelectedRows[0].Cells[0].Value;
            DialogResult result = MessageBox.Show(
                "Отменить заявку ID:" + id, 
                "Отмена", 
                MessageBoxButtons.YesNo, 
                MessageBoxIcon.Question);
            if (result == DialogResult.Yes) {
                NpgsqlTransaction trans = null;
                try {
                    string query = "update visits set status=-1 where id=" + id;
                    NpgsqlCommand command = conn.CreateCommand();
                    command.CommandText = query;
                    conn.Open();
                    trans = conn.BeginTransaction();
                    command.ExecuteNonQuery();
                    trans.Commit();
                    ShowInfo("Заявка под номером ID:" + id + " отменена");
                } catch (Exception ex) {
                    ShowError(ex.Message);
                    if (trans != null)
                        trans.Rollback();
                } finally {
                    conn.Close();
                }
                UpdateVisitTab();
            }
        }

        public void UpdateStatus() {
            int amountPatients = patientGridView.Rows.Count;
            int amountDoctors = doctorGridView.Rows.Count;
            int amountVisits = visitGridView.Rows.Count;
            statusLabel.Text = String.Format("Зарегистрировано: {0} пациентов, {1} врачей, {2} посещений", 
                amountPatients, amountDoctors, amountVisits);
        }

        public void UpdateAction(string action) {
            actionLabel.Text = action;
        }

        //-------------------------MENU--ACTIONS----------------------------
        private void switchUserMenuItem_Click(object sender, EventArgs e) {
            new AuthForm(this).ShowDialog();
        }

        private void saveMenuItem_Click(object sender, EventArgs e) {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "SQL files(*.sql)|*.sql";
            DialogResult result = saveDialog.ShowDialog();
            if (result == DialogResult.OK) {
                string filename = saveDialog.FileName;
                string pg_dump = settings.pg_home + @"\bin\pg_dump.exe";
                string host = conn.Host;
                string port = conn.Port.ToString();
                string user = conn.UserName;
                string pass = password;
                string database = conn.Database;
                string command = String.Format(
                    "\"{0}\" -F p -h {1} -p {2} -U {3} -d {4} -f {5}",
                    pg_dump,
                    host,
                    port,
                    user,
                    database,
                    filename
                );
                String batFilePath = "create_dump.bat";
                try {
                    if (!File.Exists(pg_dump))
                        throw new Exception("Исполняемый файл pg_dump.exe не найден");
                    string batContent = "set PGPASSWORD=" + pass + "\n" + command;
                    Console.WriteLine(batContent);
                    File.WriteAllText(batFilePath, batContent, Encoding.Default);
                    ProcessStartInfo procInfo = new ProcessStartInfo(batFilePath);
                    procInfo.UseShellExecute = false;
                    procInfo.CreateNoWindow = true;
                    bool status = false;
                    using (Process proc = System.Diagnostics.Process.Start(procInfo)) {
                        status = proc.WaitForExit(10000);
                        proc.Close();
                    }
                    if (!status)
                        throw new Exception("Таймаут ожидания процесса");
                    if (!File.Exists(filename))
                        throw new Exception();
                    ShowInfo("Дамп создан. База данных сохранена на компьютере", "Сохранение");
                } catch (Exception ex) {
                    ShowError("Невозможно создать дамп: " + ex.Message);
                } finally {
                    if (File.Exists(batFilePath))
                        File.Delete(batFilePath);
                }
            }
        }

        private void loadMenuItem_Click(object sender, EventArgs e) {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "SQL files(*.sql)|*.sql";
            DialogResult result = openDialog.ShowDialog();
            if (result == DialogResult.OK) {
                string filename = openDialog.FileName;
                string psql = settings.pg_home + @"\bin\psql.exe";
                string host = conn.Host;
                string port = conn.Port.ToString();
                string user = conn.UserName;
                string pass = password;
                string database = conn.Database;
                string command = String.Format(
                    "\"{0}\" -h {1} -p {2} -U {3} {4} < {5}",
                    psql,
                    host,
                    port,
                    user,
                    database,
                    filename
                );
                String batFilePath = "restore.bat";
                try {
                    if (!File.Exists(psql))
                        throw new Exception("Исполняемый файл pg_dump.exe не найден");
                    string batContent = "set PGPASSWORD=" + pass + "\n" + command;
                    Console.WriteLine(batContent);
                    File.WriteAllText(batFilePath, batContent, Encoding.Default);
                    ProcessStartInfo procInfo = new ProcessStartInfo(batFilePath);
                    procInfo.UseShellExecute = false;
                    procInfo.CreateNoWindow = true;
                    bool status = false;
                    using (Process proc = System.Diagnostics.Process.Start(procInfo)) {
                        status = proc.WaitForExit(10000);
                        proc.Close();
                    }
                    if (!status)
                        throw new Exception("Таймаут ожидания процесса");
                    if (!File.Exists(filename))
                        throw new Exception();
                    UpdatePatientTab();
                    UpdateDoctorTab();
                    UpdateVisitTab();
                    UpdateStatus();
                    UpdateAction("");
                    ShowInfo("База данных восстановлена из дампа", "Восстановление");
                } catch (Exception ex) {
                    ShowError("Невозможно восстановить базу: " + ex.Message);
                } finally {
                    if (File.Exists(batFilePath))
                        File.Delete(batFilePath);
                }
            }
        }

        private void settingsMenuItem_Click(object sender, EventArgs e) {
            new SettingsForm(settings).ShowDialog();
        }

        private void exitMenuItem_Click(object sender, EventArgs e) {
            Close();
        }

        public bool IsConnected() {
            return conn != null && conn.State == ConnectionState.Open;
        }

        public bool IsLogged() {
            return conn != null;
        }
    }
}
