﻿namespace Hospital {
    partial class DocumentForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.formBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.form1Panel = new System.Windows.Forms.Panel();
            this.patientInfoButton = new System.Windows.Forms.Button();
            this.patientBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.form2Panel = new System.Windows.Forms.Panel();
            this.doctorInfoButton = new System.Windows.Forms.Button();
            this.doctorBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.issueButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.infoLabel = new System.Windows.Forms.Label();
            this.form3Panel = new System.Windows.Forms.Panel();
            this.specBox = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.form4Panel = new System.Windows.Forms.Panel();
            this.emptyPanel = new System.Windows.Forms.Panel();
            this.infoBox = new System.Windows.Forms.RichTextBox();
            this.datePanel = new System.Windows.Forms.Panel();
            this.toYear = new System.Windows.Forms.ComboBox();
            this.fromYear = new System.Windows.Forms.ComboBox();
            this.toMonth = new System.Windows.Forms.ComboBox();
            this.toDay = new System.Windows.Forms.ComboBox();
            this.fromMonth = new System.Windows.Forms.ComboBox();
            this.fromDay = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.form1Panel.SuspendLayout();
            this.form2Panel.SuspendLayout();
            this.form3Panel.SuspendLayout();
            this.datePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // formBox
            // 
            this.formBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.formBox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.formBox.FormattingEnabled = true;
            this.formBox.Items.AddRange(new object[] {
            "F1: \"Посещения\"",
            "F2: \"Больные\"",
            "F3: \"Специальность\"",
            "F4: \"Болезни\""});
            this.formBox.Location = new System.Drawing.Point(100, 9);
            this.formBox.Name = "formBox";
            this.formBox.Size = new System.Drawing.Size(191, 24);
            this.formBox.TabIndex = 4;
            this.formBox.SelectedIndexChanged += new System.EventHandler(this.formBox_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(20, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Справка:";
            // 
            // form1Panel
            // 
            this.form1Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.form1Panel.Controls.Add(this.patientInfoButton);
            this.form1Panel.Controls.Add(this.patientBox);
            this.form1Panel.Controls.Add(this.label6);
            this.form1Panel.Location = new System.Drawing.Point(12, 127);
            this.form1Panel.Name = "form1Panel";
            this.form1Panel.Size = new System.Drawing.Size(432, 139);
            this.form1Panel.TabIndex = 6;
            // 
            // patientInfoButton
            // 
            this.patientInfoButton.Image = global::Hospital.Properties.Resources.info_icon__2_;
            this.patientInfoButton.Location = new System.Drawing.Point(392, 14);
            this.patientInfoButton.Name = "patientInfoButton";
            this.patientInfoButton.Size = new System.Drawing.Size(26, 26);
            this.patientInfoButton.TabIndex = 8;
            this.patientInfoButton.UseVisualStyleBackColor = true;
            this.patientInfoButton.Click += new System.EventHandler(this.patientInfoButton_Click);
            // 
            // patientBox
            // 
            this.patientBox.DropDownHeight = 100;
            this.patientBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.patientBox.FormattingEnabled = true;
            this.patientBox.IntegralHeight = false;
            this.patientBox.Items.AddRange(new object[] {
            "F1: \"Посещения\"",
            "F2: \"Больные\"",
            "F3: \"Специальность\"",
            "F4: \"Болезни\""});
            this.patientBox.Location = new System.Drawing.Point(87, 15);
            this.patientBox.MaxDropDownItems = 6;
            this.patientBox.Name = "patientBox";
            this.patientBox.Size = new System.Drawing.Size(299, 24);
            this.patientBox.TabIndex = 7;
            this.patientBox.Enter += new System.EventHandler(this.patientBox_Enter);
            this.patientBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.patientBox_KeyUp);
            this.patientBox.Leave += new System.EventHandler(this.patientBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(13, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Пациент:";
            // 
            // form2Panel
            // 
            this.form2Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.form2Panel.Controls.Add(this.doctorInfoButton);
            this.form2Panel.Controls.Add(this.doctorBox);
            this.form2Panel.Controls.Add(this.label13);
            this.form2Panel.Location = new System.Drawing.Point(12, 127);
            this.form2Panel.Name = "form2Panel";
            this.form2Panel.Size = new System.Drawing.Size(432, 139);
            this.form2Panel.TabIndex = 18;
            // 
            // doctorInfoButton
            // 
            this.doctorInfoButton.Image = global::Hospital.Properties.Resources.info_icon__2_;
            this.doctorInfoButton.Location = new System.Drawing.Point(392, 14);
            this.doctorInfoButton.Name = "doctorInfoButton";
            this.doctorInfoButton.Size = new System.Drawing.Size(26, 26);
            this.doctorInfoButton.TabIndex = 8;
            this.doctorInfoButton.UseVisualStyleBackColor = true;
            this.doctorInfoButton.Click += new System.EventHandler(this.doctorInfoButton_Click);
            // 
            // doctorBox
            // 
            this.doctorBox.DropDownHeight = 100;
            this.doctorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.doctorBox.FormattingEnabled = true;
            this.doctorBox.IntegralHeight = false;
            this.doctorBox.Items.AddRange(new object[] {
            "F1: \"Посещения\"",
            "F2: \"Больные\"",
            "F3: \"Специальность\"",
            "F4: \"Болезни\""});
            this.doctorBox.Location = new System.Drawing.Point(87, 15);
            this.doctorBox.MaxDropDownItems = 6;
            this.doctorBox.Name = "doctorBox";
            this.doctorBox.Size = new System.Drawing.Size(299, 24);
            this.doctorBox.TabIndex = 7;
            this.doctorBox.Enter += new System.EventHandler(this.doctorBox_Enter);
            this.doctorBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.doctorBox_KeyUp);
            this.doctorBox.Leave += new System.EventHandler(this.doctorBox_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(13, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 16);
            this.label13.TabIndex = 7;
            this.label13.Text = "Врач:";
            // 
            // issueButton
            // 
            this.issueButton.Enabled = false;
            this.issueButton.Location = new System.Drawing.Point(288, 272);
            this.issueButton.Name = "issueButton";
            this.issueButton.Size = new System.Drawing.Size(75, 25);
            this.issueButton.TabIndex = 7;
            this.issueButton.Text = "Выдать";
            this.issueButton.UseVisualStyleBackColor = true;
            this.issueButton.Click += new System.EventHandler(this.issueButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(369, 272);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 25);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoLabel.Location = new System.Drawing.Point(20, 44);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(67, 15);
            this.infoLabel.TabIndex = 9;
            this.infoLabel.Text = "Описание:";
            // 
            // form3Panel
            // 
            this.form3Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.form3Panel.Controls.Add(this.specBox);
            this.form3Panel.Controls.Add(this.label17);
            this.form3Panel.Location = new System.Drawing.Point(12, 127);
            this.form3Panel.Name = "form3Panel";
            this.form3Panel.Size = new System.Drawing.Size(432, 139);
            this.form3Panel.TabIndex = 19;
            // 
            // specBox
            // 
            this.specBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.specBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specBox.FormattingEnabled = true;
            this.specBox.Items.AddRange(new object[] {
            "F1: \"Посещения\"",
            "F2: \"Больные\"",
            "F3: \"Специальность\"",
            "F4: \"Болезни\""});
            this.specBox.Location = new System.Drawing.Point(126, 15);
            this.specBox.Name = "specBox";
            this.specBox.Size = new System.Drawing.Size(293, 24);
            this.specBox.TabIndex = 7;
            this.specBox.Enter += new System.EventHandler(this.specBox_Enter);
            this.specBox.Leave += new System.EventHandler(this.specBox_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(13, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(112, 16);
            this.label17.TabIndex = 7;
            this.label17.Text = "Специальность:";
            // 
            // form4Panel
            // 
            this.form4Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.form4Panel.Location = new System.Drawing.Point(12, 127);
            this.form4Panel.Name = "form4Panel";
            this.form4Panel.Size = new System.Drawing.Size(432, 139);
            this.form4Panel.TabIndex = 20;
            // 
            // emptyPanel
            // 
            this.emptyPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.emptyPanel.Location = new System.Drawing.Point(12, 127);
            this.emptyPanel.Name = "emptyPanel";
            this.emptyPanel.Size = new System.Drawing.Size(432, 139);
            this.emptyPanel.TabIndex = 21;
            // 
            // infoBox
            // 
            this.infoBox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoBox.Location = new System.Drawing.Point(12, 62);
            this.infoBox.Name = "infoBox";
            this.infoBox.ReadOnly = true;
            this.infoBox.Size = new System.Drawing.Size(432, 49);
            this.infoBox.TabIndex = 22;
            this.infoBox.Text = "";
            // 
            // datePanel
            // 
            this.datePanel.Controls.Add(this.toYear);
            this.datePanel.Controls.Add(this.fromYear);
            this.datePanel.Controls.Add(this.toMonth);
            this.datePanel.Controls.Add(this.toDay);
            this.datePanel.Controls.Add(this.fromMonth);
            this.datePanel.Controls.Add(this.fromDay);
            this.datePanel.Controls.Add(this.label1);
            this.datePanel.Controls.Add(this.label2);
            this.datePanel.Controls.Add(this.label3);
            this.datePanel.Location = new System.Drawing.Point(19, 181);
            this.datePanel.Name = "datePanel";
            this.datePanel.Size = new System.Drawing.Size(297, 71);
            this.datePanel.TabIndex = 23;
            // 
            // toYear
            // 
            this.toYear.DropDownHeight = 130;
            this.toYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toYear.FormattingEnabled = true;
            this.toYear.IntegralHeight = false;
            this.toYear.Location = new System.Drawing.Point(220, 37);
            this.toYear.Name = "toYear";
            this.toYear.Size = new System.Drawing.Size(67, 24);
            this.toYear.TabIndex = 26;
            this.toYear.SelectedIndexChanged += new System.EventHandler(this.toYear_SelectedIndexChanged);
            // 
            // fromYear
            // 
            this.fromYear.DropDownHeight = 130;
            this.fromYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fromYear.FormattingEnabled = true;
            this.fromYear.IntegralHeight = false;
            this.fromYear.Location = new System.Drawing.Point(220, 7);
            this.fromYear.Name = "fromYear";
            this.fromYear.Size = new System.Drawing.Size(67, 24);
            this.fromYear.TabIndex = 25;
            this.fromYear.SelectedIndexChanged += new System.EventHandler(this.fromYear_SelectedIndexChanged);
            // 
            // toMonth
            // 
            this.toMonth.DropDownHeight = 130;
            this.toMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toMonth.FormattingEnabled = true;
            this.toMonth.IntegralHeight = false;
            this.toMonth.Location = new System.Drawing.Point(170, 37);
            this.toMonth.Name = "toMonth";
            this.toMonth.Size = new System.Drawing.Size(44, 24);
            this.toMonth.TabIndex = 24;
            this.toMonth.SelectedIndexChanged += new System.EventHandler(this.toMonth_SelectedIndexChanged);
            // 
            // toDay
            // 
            this.toDay.DropDownHeight = 130;
            this.toDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toDay.FormattingEnabled = true;
            this.toDay.IntegralHeight = false;
            this.toDay.Location = new System.Drawing.Point(120, 37);
            this.toDay.Name = "toDay";
            this.toDay.Size = new System.Drawing.Size(44, 24);
            this.toDay.TabIndex = 23;
            // 
            // fromMonth
            // 
            this.fromMonth.DropDownHeight = 130;
            this.fromMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fromMonth.FormattingEnabled = true;
            this.fromMonth.IntegralHeight = false;
            this.fromMonth.Location = new System.Drawing.Point(169, 7);
            this.fromMonth.Name = "fromMonth";
            this.fromMonth.Size = new System.Drawing.Size(44, 24);
            this.fromMonth.TabIndex = 22;
            this.fromMonth.SelectedIndexChanged += new System.EventHandler(this.fromMonth_SelectedIndexChanged);
            // 
            // fromDay
            // 
            this.fromDay.DropDownHeight = 130;
            this.fromDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fromDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fromDay.FormattingEnabled = true;
            this.fromDay.IntegralHeight = false;
            this.fromDay.Location = new System.Drawing.Point(119, 7);
            this.fromDay.Name = "fromDay";
            this.fromDay.Size = new System.Drawing.Size(44, 24);
            this.fromDay.TabIndex = 21;
            this.fromDay.SelectedIndexChanged += new System.EventHandler(this.fromDay_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(81, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 16);
            this.label1.TabIndex = 20;
            this.label1.Text = "по :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(81, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "с :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(7, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 16);
            this.label3.TabIndex = 18;
            this.label3.Text = "Период :";
            // 
            // DocumentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 308);
            this.Controls.Add(this.datePanel);
            this.Controls.Add(this.infoBox);
            this.Controls.Add(this.form2Panel);
            this.Controls.Add(this.form4Panel);
            this.Controls.Add(this.emptyPanel);
            this.Controls.Add(this.form3Panel);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.issueButton);
            this.Controls.Add(this.form1Panel);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.formBox);
            this.Name = "DocumentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Справки";
            this.form1Panel.ResumeLayout(false);
            this.form1Panel.PerformLayout();
            this.form2Panel.ResumeLayout(false);
            this.form2Panel.PerformLayout();
            this.form3Panel.ResumeLayout(false);
            this.form3Panel.PerformLayout();
            this.datePanel.ResumeLayout(false);
            this.datePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox formBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel form1Panel;
        private System.Windows.Forms.Panel form2Panel;
        private System.Windows.Forms.Button doctorInfoButton;
        private System.Windows.Forms.ComboBox doctorBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button patientInfoButton;
        private System.Windows.Forms.ComboBox patientBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button issueButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Panel form3Panel;
        private System.Windows.Forms.Panel form4Panel;
        private System.Windows.Forms.ComboBox specBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel emptyPanel;
        private System.Windows.Forms.RichTextBox infoBox;
        private System.Windows.Forms.Panel datePanel;
        private System.Windows.Forms.ComboBox toYear;
        private System.Windows.Forms.ComboBox fromYear;
        private System.Windows.Forms.ComboBox toMonth;
        private System.Windows.Forms.ComboBox toDay;
        private System.Windows.Forms.ComboBox fromMonth;
        private System.Windows.Forms.ComboBox fromDay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}