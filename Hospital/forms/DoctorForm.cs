﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Npgsql;

namespace Hospital {
    public partial class DoctorForm : Form {
        private NpgsqlConnection conn;
        private MainForm form;
        private bool done = false;
        private DateTime now;

        private int id = -1;
        private bool mode;

        private int minYear = 1900;

        public DoctorForm(MainForm form, NpgsqlConnection conn) {
            InitializeComponent();
            this.form = form;
            this.conn = conn;

            now = DateTime.Now;
            for (int i = now.Year; i >= minYear; i--)
                yearBox.Items.Add(i);
            for (int i = 1; i <= 12; i++)
                monthBox.Items.Add(i);
            yearBox.SelectedIndex = yearBox.Items.IndexOf(2000);
            monthBox.SelectedIndex = 0;

            try {
                string query = "select name from specs";
                NpgsqlDataAdapter adapter = new NpgsqlDataAdapter();
                adapter.SelectCommand = new NpgsqlCommand(query, conn);
                DataSet set = new DataSet();
                conn.Open();
                adapter.Fill(set);
                DataTable table = set.Tables[0];
                if (table.Rows.Count == 0)
                    throw new Exception("Таблица \"specs\" пуста. Обратитесь к администратору");
                foreach (DataRow row in table.Rows) { 
                    if (row.ItemArray[0] != DBNull.Value)
                        specBox.Items.Add(((string)row.ItemArray[0]).Trim());
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } finally {
                conn.Close();
            }
        }

        public DialogResult AddDoctor() {
            Text = "Новый сотрудник";
            mode = true;

            return ShowDialog();
        }

        public DialogResult EditDoctor(Doctor doctor) {
            Text = "Сотрудник \"" + doctor.Name + "\"";
            mode = false;

            id = doctor.ID;
            nameBox.Text = doctor.Name;
            roomBox.Text = doctor.Room.ToString();
            string spec = doctor.Spec;
            
            specBox.SelectedIndex = specBox.Items.IndexOf(spec);

            string[] date = doctor.Date.Split('-');
            int day = Int32.Parse(date[0]);
            int month = Int32.Parse(date[1]);
            int year = Int32.Parse(date[2]);

            yearBox.SelectedIndex = now.Year - year;
            monthBox.SelectedIndex = month - 1;
            dayBox.SelectedIndex = day - 1;

            return ShowDialog();
        }

        private void saveButton_Click(object sender, EventArgs e) {
            string name = nameBox.Text.Trim();
            string spec = specBox.Text.Trim();
            string date = (dayBox.SelectedIndex + 1) + "-" + (monthBox.SelectedIndex + 1) + "-" + (now.Year - yearBox.SelectedIndex);
            int room = Int32.Parse(roomBox.Text.Trim());
            
            Doctor doctor = new Doctor(id, name, spec, room, date);

            if (mode)
                form.AddDoctor(doctor);
            else
                form.EditDoctor(doctor);

            done = true;
            Close();

        }

        private void cancelButton_Click(object sender, EventArgs e) {
            done = true;
            Close();
        }

        private void DoctorForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (!done && nameBox.Text.Trim().Length > 0) {
                DialogResult result = MessageBox.Show("Сохранить изменения?", "Выход", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes) {
                    saveButton.PerformClick();
                } else if (result == DialogResult.Cancel) {
                    e.Cancel = true;
                }
            }
        }

        private void monthBox_SelectedIndexChanged(object sender, EventArgs e) {
            int month = monthBox.SelectedIndex + 1;
            int year = now.Year - yearBox.SelectedIndex;
            int days = (year == now.Year) ? now.Day : DateTime.DaysInMonth(year, month);

            dayBox.Items.Clear();
            for (int i = 1; i <= days; i++)
                dayBox.Items.Add(i);

            dayBox.SelectedIndex = 0;
        }

        private void yearBox_SelectedIndexChanged(object sender, EventArgs e) {
            dayBox.Items.Clear();
            for (int i = 1; i <= 31; i++)
                dayBox.Items.Add(i);

            monthBox.Items.Clear();
            int year = now.Year - yearBox.SelectedIndex;
            int month = (year == now.Year) ? now.Month : 12;
            for (int i = 1; i <= month; i++)
                monthBox.Items.Add(i);
            monthBox.SelectedIndex = 0;
        }

        private void nameBox_TextChanged(object sender, EventArgs e) {
            saveButton.Enabled = nameBox.Text.Length > 0;
        }

        private void phoneBox_KeyPress(object sender, KeyPressEventArgs e) {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }  
    }
}
