﻿namespace Hospital {
    partial class VisitForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.minBox = new System.Windows.Forms.TextBox();
            this.hourBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.monthBox = new System.Windows.Forms.ComboBox();
            this.yearBox = new System.Windows.Forms.ComboBox();
            this.dayBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.doctorBox = new System.Windows.Forms.ComboBox();
            this.specBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.addPatientButton = new System.Windows.Forms.Button();
            this.nameBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Заявка на приём к врачу";
            // 
            // panel
            // 
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Controls.Add(this.minBox);
            this.panel.Controls.Add(this.hourBox);
            this.panel.Controls.Add(this.label5);
            this.panel.Controls.Add(this.monthBox);
            this.panel.Controls.Add(this.yearBox);
            this.panel.Controls.Add(this.dayBox);
            this.panel.Controls.Add(this.label4);
            this.panel.Controls.Add(this.doctorBox);
            this.panel.Controls.Add(this.specBox);
            this.panel.Controls.Add(this.label3);
            this.panel.Controls.Add(this.addPatientButton);
            this.panel.Controls.Add(this.nameBox);
            this.panel.Controls.Add(this.label2);
            this.panel.Controls.Add(this.label6);
            this.panel.Location = new System.Drawing.Point(13, 33);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(494, 150);
            this.panel.TabIndex = 1;
            // 
            // minBox
            // 
            this.minBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minBox.Location = new System.Drawing.Point(424, 107);
            this.minBox.Name = "minBox";
            this.minBox.Size = new System.Drawing.Size(61, 24);
            this.minBox.TabIndex = 15;
            // 
            // hourBox
            // 
            this.hourBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hourBox.Location = new System.Drawing.Point(351, 107);
            this.hourBox.Name = "hourBox";
            this.hourBox.Size = new System.Drawing.Size(61, 24);
            this.hourBox.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(293, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Время:";
            // 
            // monthBox
            // 
            this.monthBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.monthBox.FormattingEnabled = true;
            this.monthBox.Location = new System.Drawing.Point(132, 107);
            this.monthBox.Name = "monthBox";
            this.monthBox.Size = new System.Drawing.Size(45, 24);
            this.monthBox.TabIndex = 10;
            this.monthBox.SelectedIndexChanged += new System.EventHandler(this.monthBox_SelectedIndexChanged);
            // 
            // yearBox
            // 
            this.yearBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yearBox.FormattingEnabled = true;
            this.yearBox.Location = new System.Drawing.Point(183, 107);
            this.yearBox.Name = "yearBox";
            this.yearBox.Size = new System.Drawing.Size(77, 24);
            this.yearBox.TabIndex = 9;
            this.yearBox.SelectedIndexChanged += new System.EventHandler(this.yearBox_SelectedIndexChanged);
            // 
            // dayBox
            // 
            this.dayBox.DropDownHeight = 140;
            this.dayBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayBox.FormattingEnabled = true;
            this.dayBox.IntegralHeight = false;
            this.dayBox.Location = new System.Drawing.Point(81, 107);
            this.dayBox.Name = "dayBox";
            this.dayBox.Size = new System.Drawing.Size(45, 24);
            this.dayBox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(32, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Дата:";
            // 
            // doctorBox
            // 
            this.doctorBox.DropDownHeight = 200;
            this.doctorBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.doctorBox.FormattingEnabled = true;
            this.doctorBox.IntegralHeight = false;
            this.doctorBox.Location = new System.Drawing.Point(81, 61);
            this.doctorBox.Name = "doctorBox";
            this.doctorBox.Size = new System.Drawing.Size(264, 24);
            this.doctorBox.TabIndex = 5;
            this.doctorBox.Enter += new System.EventHandler(this.doctorBox_Enter);
            this.doctorBox.Leave += new System.EventHandler(this.doctorBox_Leave);
            // 
            // specBox
            // 
            this.specBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specBox.FormattingEnabled = true;
            this.specBox.Location = new System.Drawing.Point(351, 61);
            this.specBox.Name = "specBox";
            this.specBox.Size = new System.Drawing.Size(134, 24);
            this.specBox.TabIndex = 4;
            this.specBox.TextChanged += new System.EventHandler(this.specBox_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(17, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Доктор:";
            // 
            // addPatientButton
            // 
            this.addPatientButton.Image = global::Hospital.Properties.Resources._new;
            this.addPatientButton.Location = new System.Drawing.Point(460, 11);
            this.addPatientButton.Name = "addPatientButton";
            this.addPatientButton.Size = new System.Drawing.Size(25, 25);
            this.addPatientButton.TabIndex = 2;
            this.addPatientButton.UseVisualStyleBackColor = true;
            this.addPatientButton.Click += new System.EventHandler(this.addPatientButton_Click);
            // 
            // nameBox
            // 
            this.nameBox.DropDownHeight = 100;
            this.nameBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameBox.FormattingEnabled = true;
            this.nameBox.IntegralHeight = false;
            this.nameBox.Location = new System.Drawing.Point(81, 12);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(373, 24);
            this.nameBox.TabIndex = 1;
            this.nameBox.Enter += new System.EventHandler(this.nameBox_Enter);
            this.nameBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nameBox_KeyUp);
            this.nameBox.Leave += new System.EventHandler(this.nameBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(7, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Пациент:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(412, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 23);
            this.label6.TabIndex = 14;
            this.label6.Text = ":";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(433, 193);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 25);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(351, 193);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 25);
            this.saveButton.TabIndex = 3;
            this.saveButton.Text = "Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // VisitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 227);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(535, 266);
            this.MinimumSize = new System.Drawing.Size(535, 266);
            this.Name = "VisitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Приём";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VisitForm_FormClosing);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addPatientButton;
        private System.Windows.Forms.ComboBox nameBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox doctorBox;
        private System.Windows.Forms.ComboBox specBox;
        private System.Windows.Forms.ComboBox monthBox;
        private System.Windows.Forms.ComboBox yearBox;
        private System.Windows.Forms.ComboBox dayBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.TextBox minBox;
        private System.Windows.Forms.TextBox hourBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}